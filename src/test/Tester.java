package test;

import com.mindrop.preprocessing.localization.QueryEntityLocationRecognitionFactory;
import com.mindrop.preprocessing.localization.QueryEntityLocationRecognitionModel;
import java.util.List;

public class Tester {

    public static void main(String[] args) {
        String query1 = "2010 [UK) ,  venezolano dfdfd  principado de asturias asturias dfdfdfcomercio tennesianas andalucía y ecuador riojan tambien comunidad valenciana ok comunitat valenciana";
        String query2 = "ESTADOS UNIDOS MERCADO texanas yibutiana dakotano riojan canarian canadá EEUU eeuu islas baleares uk END 345";

        QueryEntityLocationRecognitionFactory factory
                = new QueryEntityLocationRecognitionFactory(QueryEntityLocationRecognitionFactory.SMART_LOCATIONS_RECOGNITION);
        QueryEntityLocationRecognitionModel model = factory.createQueryEntityLocationRecognitionModel();

        List<String> locations1 = model.getSegments(query1).getLocations();
        String remainder1 = model.getSegments(query1).getResidualQuery();

        System.out.println(locations1);
        System.out.println(remainder1);

        List<String> locations2 = model.getSegments(query2).getLocations();
        String remainder2 = model.getSegments(query2).getResidualQuery();

        System.out.println(locations2);
        System.out.println(remainder2);
    }

}
