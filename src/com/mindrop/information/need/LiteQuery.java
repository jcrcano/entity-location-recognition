/**
 * Copyright (c) 2014 Findrop. All rights reserved.
 *
 * The contents of this file are subject under the terms described in the
 * FINDROP_LICENSE file included in this distribution; you may not use this
 * file except in compliance with the License.
 *
 * @author BOTELLA-CORBI, BERNARDO
 * @author RODRIGUEZ-CANO, JULIO C.
 * @version 1.0
 * @since 1.0
 */

package com.mindrop.information.need;

import com.mindrop.preprocessing.analysis.TermAnalyzer;
import com.mindrop.preprocessing.term.ContextTerm;

import java.util.ArrayList;
import java.util.List;

public class LiteQuery implements Query {

    private static final String SOLR_RESERVED_CHARACTERS = "¡!¿?;+-&|(){}[]^\"~*?:/";

    private String query;

    public LiteQuery(String query) {
        this.query = query;
    }

    public LiteQuery() {
        this.query = "";
    }

    public String getQuery() {
        return query;
    }

    @Override
    public String[] getTokens() {
        String[] result = null;
        List<String> terms = new ArrayList<>();

        String[] tokens = this.query.split(" |\\(|\\)|!|¡|¿|\\?|\\{|\\}|\\*|:|;|\\[|\\]|\"|\\^|~|-");

        for (String token : tokens) {
            if (ContextTerm.isValidTerm(token)) {
                terms.add(TermAnalyzer.replaceIllegalCharacter(token));
            }
        }

        int count = terms.size();
        result = new String[count];

        for (int i = 0; i < count; ++i) {
            result[i] = terms.get(i);
        }

        return result;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LiteQuery queryLite = (LiteQuery) o;

        if (query != null ? !query.equals(queryLite.query) : queryLite.query != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return query != null ? query.hashCode() : 0;
    }

    @Override
    public String toString() {
        return query;
    }
}
