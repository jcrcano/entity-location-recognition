/**
 * Copyright (c) 2014 Findrop. All rights reserved.
 *
 * The contents of this file are subject under the terms described in the
 * FINDROP_LICENSE file included in this distribution; you may not use this
 * file except in compliance with the License.
 *
 * @author BOTELLA-CORBI, BERNARDO
 * @author RODRIGUEZ-CANO, JULIO C.
 * @version 1.0
 * @since 1.0
 */

package com.mindrop.information.need;

public interface Query {

    public static final int MAX_QUERY_LENGTH = 10;

    public void setQuery(String query);

    public String getQuery();

    public String[] getTokens();

}
