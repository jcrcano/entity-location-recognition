package com.mindrop.preprocessing.analysis;

public class TermAnalyzer {

    private static final String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
    private static final String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";

    public static String replaceIllegalCharacter(String input) {
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output.toLowerCase();
    }

}
