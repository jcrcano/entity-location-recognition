package com.mindrop.preprocessing.analysis;

public interface LocationAnalyzer {
    public String[] tokenStream(String stream);
}
