package com.mindrop.preprocessing.analysis;

public class LocationQueryAnalyzer implements LocationAnalyzer{
    private static final String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
    private static final String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";


    public LocationQueryAnalyzer (){}

    @Override
    public String[] tokenStream(String stream) {
        String[] result = stream.split(" |\n|\t|\\.|;|,|:|\\(|\\)|-|\"|\'|<|>|\\[|\\]");

        for (int i = 0; i < result.length; ++i) {
            result[i] = replaceIllegalCharacter(result[i]);
        }

        return result;
    }

    public static String replaceIllegalCharacter(String input) {
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }

}
