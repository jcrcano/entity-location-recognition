package com.mindrop.preprocessing.analysis;

public class LocationCorpusAnalyzer implements LocationAnalyzer{
    private static final String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
    private static final String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";

    @Override
    public String[] tokenStream(String stream) {
        String[] result = stream.split(" |\\n|,");

        for (int i = 0; i < result.length; ++i) {
            result[i] = remova1(result[i]);
        }

        return result;
    }

    private String remova1(String input) {
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }
        return output;
    }
}
