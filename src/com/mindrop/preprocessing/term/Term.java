/**
 * Copyright (c) 2014 Findrop. All rights reserved.
 *
 * The contents of this file are subject under the terms described in the
 * FINDROP_LICENSE file included in this distribution; you may not use this
 * file except in compliance with the License.
 *
 * @author BOTELLA-CORBI, BERNARDO
 * @author RODRIGUEZ-CANO, JULIO C.
 * @version 1.0
 * @since 1.0
 */

package com.mindrop.preprocessing.term;

public abstract class Term implements ValidableTerm{

    public abstract boolean isValidTerm();


}
