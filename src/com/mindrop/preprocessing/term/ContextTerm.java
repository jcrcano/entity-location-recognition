/**
 * Copyright (c) 2014 Findrop. All rights reserved.
 *
 * The contents of this file are subject under the terms described in the
 * FINDROP_LICENSE file included in this distribution; you may not use this
 * file except in compliance with the License.
 *
 * @author BOTELLA-CORBI, BERNARDO
 * @author RODRIGUEZ-CANO, JULIO C.
 * @version 1.0
 * @since 1.0
 */

package com.mindrop.preprocessing.term;

import java.util.regex.Pattern;

public class ContextTerm extends Term implements ValidableTerm {
    private static final int MAX_NUMBER_LENGTH = 7;
    private static final int MAX_WORD_LENGTH = 30;
    private static final int MAX_WORD_NUMBER_LENGTH = 6;
    private static final int MAX_DECIMAL_PART_LENGTH = 3;

    private String term;

    public ContextTerm(String term) {
        this.term = term;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean isValidTerm() {
        return isValidTerm(getTerm());
    }


    public static boolean isValidTerm(String term) {

        return isValidWord(term) || isValidInteger(term) || isValidNumber(term);
    }

    public static boolean isValidWord(String term) {
        String patternWord = "[A-Za-zñÑáéíóúÁÉÍÓÚçÇ]+";
        String patternLength = ".{1," + MAX_WORD_LENGTH + "}";

        String patternWordSN = "[0-9A-Za-zñÑáéíóúÁÉÍÓÚçÇ]+";
        String patternLengthSN = ".{1," + MAX_WORD_NUMBER_LENGTH + "}";

        return (Pattern.matches(patternWord, term) && Pattern.matches(patternLength, term)) || (Pattern.matches(patternWordSN, term) && Pattern.matches(patternLengthSN, term));
    }

    public static boolean isValidNumber(String term) {
        String patternNumber = "(.{1})([0-9]{1,7})(([\\.,]{1})([0-9]{1," + MAX_DECIMAL_PART_LENGTH + "}))*([MmKkBbΔ]{0,2}|EUR)";

        return Pattern.matches(patternNumber, term) && isValidNumberLength(term);
    }

    public static boolean isValidInteger(String term) {
        String patternInt = "(.{1})([0-9]{1,7}[MmKkBb]{0,2})";
        return Pattern.matches(patternInt, term);
    }

    private static boolean isValidNumberLength(String term) {
        int digitCount = 0;

        for (int i = 0; i < term.length(); ++i) {
            if (isNumeric(term.charAt(i))) {
                ++digitCount;
            }
        }

        return (digitCount <= MAX_NUMBER_LENGTH);
    }

    private static boolean isValidNumberLength(String term, int length) {
        int digitCount = 0;

        for (int i = 0; i < term.length(); ++i) {
            if (isNumeric(term.charAt(i))) {
                ++digitCount;
            }
        }

        return digitCount <= length;
    }

    private static boolean isNumeric(char ch) {
        Character objCh = new Character(ch);
        try {
            Integer.parseInt(objCh.toString());
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }


}
