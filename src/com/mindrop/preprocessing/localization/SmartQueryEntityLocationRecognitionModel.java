package com.mindrop.preprocessing.localization;

import com.mindrop.information.need.LiteQuery;
import com.mindrop.information.need.Query;
import com.mindrop.preprocessing.analysis.LocationQueryAnalyzer;

import java.util.*;
import java.util.regex.Pattern;

public class SmartQueryEntityLocationRecognitionModel implements QueryEntityLocationRecognitionModel {

    @Override
    public LocationQuerySegmentation getSegments(String rawQuery) {
        NamedLocationRecognition locations = NamedLocationRecognition.getInstance();
        //Set<String> locationEntities = locations.extractAllLocationsFromQuery(rawQuery);

        List<Location> locs = locations.extractSimpleLocationsCaseInsensitive(rawQuery, Location.SPA_LANG, new LocationQueryAnalyzer());
        //System.out.println("LOCATIONS: " + locs);

        Set<String> locationEntities = new HashSet<>();

        for (Location l : locs) {
            locationEntities.add(l.getName());
        }

        //System.out.println(locationEntities);
        LocationQuerySegmentation result = new LocationQuerySegmentation();
        String splitStrQuery = splitLocationsAsStr(new LiteQuery(rawQuery), locationEntities);

        locationEntities = mapGentilics(locationEntities);

        result.getLocations().addAll(locationEntities);
        result.setResidualQuery(splitStrQuery);

        return result;
    }

    private Set<String> mapGentilics(Set<String> rawLocations) {
        Set<String> result = new HashSet<>();

        for (String loc : rawLocations) {
            if (NamedLocationRecognition.getInstance().gentilics.containsKey(loc)) {
                result.add(NamedLocationRecognition.getInstance().gentilics.get(loc));
            } else {
                result.add(loc);
            }
        }

        return result;

    }

    private String splitLocationsAsStr(Query query, Set<String> locationEntities) {
        String[] tokens = query.getTokens();
        String result = "";

        List<String> arr = new ArrayList<>();
        arr.addAll(locationEntities);

        Set<String> locs = new HashSet<>();

        for (int i = 0; i < arr.size(); ++i) {
            locs.addAll(Arrays.asList(arr.get(i).split(" ")));
        }

        for (String token : tokens) {
            token = LocationQueryAnalyzer.replaceIllegalCharacter(token);

            if (!locs.contains(token.toLowerCase()) && !locs.contains(token.toUpperCase())) {
                result += token + " ";
            }
        }

        return result.trim();
    }

    private boolean isCorrectYearFormat(String token) {
        return (Pattern.matches("^\\d{4}$", token) && (Integer.parseInt(token) <= Calendar.getInstance().get(Calendar.YEAR) + 30) && (Integer.parseInt(token) >= Calendar.getInstance().get(Calendar.YEAR) - 30));
    }

}
