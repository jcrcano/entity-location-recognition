package com.mindrop.preprocessing.localization;

import com.mindrop.preprocessing.analysis.LocationAnalyzer;
import com.mindrop.preprocessing.analysis.LocationCorpusAnalyzer;
import com.mindrop.preprocessing.analysis.LocationQueryAnalyzer;

import java.util.*;

/**
 * A MultiKeyHashtable.
 */
public class NamedLocationRecognition {

    private static NamedLocationRecognition instance = new NamedLocationRecognition();

    public static synchronized NamedLocationRecognition getInstance() {
        return instance;
    }

    private static final int INITIAL_CAPACITY_FOR_CONTRIES = 500;
    private static final int INITIAL_CAPACITY_FOR_ADMIN_DIV_FL = 1000;
    private static final int INITIAL_CAPACITY_FOR_ADMIN_DIV_SL = 2000;
    private static final int INITIAL_CAPACITY_FOR_ACRONYMS = 100;

    private static final int NAME_MAX_LENGTH = 8;

    Map<String, Country> spaCountriesMap;
    Map<String, AdminDivisionFirstLevel> spaFirstLevelLocationMap;
    Map<String, AdminDivisionSecondLevel> spaSecondLevelLocationMap;
    Map<String, Acronym> spaAcronymLocationMap;
    Map<String, Capital> spaCapitalLocationMap;

    Map<String, Country> engCountriesMap;
    Map<String, AdminDivisionFirstLevel> engFirstLevelLocationMap;
    Map<String, AdminDivisionSecondLevel> engSecondLevelLocationMap;
    Map<String, Acronym> engAcronymLocationMap;
    Map<String, Capital> engCapitalLocationMap;

    Map<String, String> gentilics;

    public String[][] tableSpaAdminDivFirstLevel = null;

    public String[][] tableEngAdminDivFirstLevel = null;

    private NamedLocationRecognition() {

        String[] spaCountries = {"afganistan", "iles aland", "albania", "argelia", "samoa americana", "andorra",
            "angola", "anguila", "antartida", "antigua y barbuda", "argentina", "armenia", "aruba", "australia",
            "austria", "azerbaiyan", "bahamas", "bahrein", "bangladesh", "barbados", "belarus", "belgica",
            "belice", "benin", "bermudes", "bhutan", "bolivia", "bonaire", "bosnia y herzegovina", "botswana",
            "ile bouvet", "brasil", "territoire britannique de l ocean indien", "brunei darussalam", "bulgaria",
            "burkina faso", "burundi", "camboya", "camerun", "canada", "cabo verde", "iles caimanes",
            "republique centrafricaine", "chad", "chile", "china", "ile christmas", "iles cocos", "keeling",
            "colombia", "comoras", "congo", "republica democratica del congo", "iles cook", "costa rica",
            "cote d ivoire", "croacia", "cuba", "curacao", "chipre", "republique tcheque", "dinamarca", "djibouti",
            "dominica", "dominicaine republique", "ecuador", "egipto", "el salvador", "guinea ecuatorial",
            "eritrea", "estonia", "etiopia", "iles falkland", "malvinas", "iles feroe", "fiji", "finlandia",
            "francia", "guayana francesa", "polinesia francesa", "territorios australes franceses", "gabón",
            "gambia", "georgia", "alemania", "ghana", "gibraltar", "grecia", "groenlandia", "guadalupe", "granada",
            "guam", "guatemala", "guernsey", "guinea", "guinea bissau", "guyana", "haiti", "iles santa sede",
            "honduras", "hong kong", "hungria", "islandia", "india", "indonesia", "iran", "iraq", "irlanda",
            "isla de man", "israel", "italia", "jamaica", "japon", "jersey", "jordania", "kazajstan", "kenya",
            "kiribati", "coree", "coree", "kuwait", "kirguistan", "republique democratique populaire lao",
            "letonia", "libano", "lesotho", "liberia", "jamahiriya arabe libyenne", "liechtenstein", "lituania",
            "luxemburgo", "macao", "macedoine", "madagascar", "malawi", "malasia", "maldivas", "mali", "malta",
            "iles marshall", "martinica", "mauritania", "mauricio", "mayotte", "mexico", "micronesie", "moldova",
            "monaco", "mongolia", "montenegro", "montserrat", "marruecos", "mozambique", "myanmar", "namibia",
            "nauru", "nepal", "paises bajos", "nueva caledonia", "nueva zelandia", "nicaragua", "níger", "nigeria",
            "niue", "ile norfolk", "iles mariannes du nord", "noruega", "territori palestina ocupat", "oman",
            "pakistan", "palau", "panama", "papua nueva guinea", "paraguay", "peru", "filipinas", "pitcairn",
            "polonia", "portugal", "puerto rico", "qatar", "isla reunion", "rumania", "federation de russie",
            "rwanda", "saint barthelemy", "saint helena", "saint kitts y nevis", "santa lucia", "saint martin",
            "san pedro y miquelon", "san vicente y las granadinas", "samoa", "san marino", "santo tome y principe",
            "arabia saudita", "arabia saudi", "senegal", "serbia", "seychelles", "sierra leona", "singapur", "san martin",
            "republica eslovaca", "eslovenia", "iles salomon", "somali", "sudafrica",
            "islas georgias del sur y sandwich del sur", "sudan del sur", "espana", "sri lanka", "sudan",
            "suriname", "islas svalbard y jan mayen", "swazilandia", "suecia", "suiza", "republique arabe syrienne",
            "taiwan", "tayikistan", "tanzanie", "tailandia", "timor leste", "togo", "tokelau", "tonga",
            "trinidad y tabago", "tunez", "turquia", "turkmenistan", "iles turks et caiques", "tuvalu", "uganda",
            "ucrania", "emiratos arabes unidos", "el reino unido de gran bretana e irlanda del norte",
            "reino unido", "estados unidos de america", "estados unidos", "estados unidos islas menores",
            "uruguay", "uzbekistan", "vanuatu", "venezuela", "viet nam", "iles vierges britanniques",
            "iles vierges des etats unis", "islas wallis y futuna", "sahara occidental", "yemen", "zambia",
            "zimbabwe", "commonwealth", "movimiento de paises no alineados", "union africana",
            "african caribbean and pacific group of states", "economic community of west african states",
            "monetary and economic community of central africa", "southern african development community",
            "the east african community", "the indian ocean commission", "comunidad andina de naciones",
            "federacion iberoamericana de ombudsman", "organizacion de los estados americanos",
            "organization of eastern caribbean states", "grupo de rio", "association of southeast asian nations",
            "Pacific Islands Forum", "south asian association for regional cooperation", "consejo de europa",
            "union europea", "organización para la seguridad y la cooperación en europa",
            "gulf cooperation council", "liga de estados arabes", "africa", "america", "las americas", "europa",
            "asia", "oceania", "antartida", "europe", "america del norte", "america del sur", "america central",
            "antillas", "caribe", "north america", "south america", "caribbean sea", "caribbean",
            "central american", "africa del norte", "africa del sur", "africa del este", "africa del oeste",
            "africa central", "north africa", "south africa", "central africa", "middle africa", "east africa",
            "eastern africa", "west africa", "western africa", "europa del norte", "europa del sur",
            "europa del este", "europa del oeste", "europa central", "europa oriental", "europa occidental",
            "africa oriental", "africa occidental", "america oriental", "america occidental", "asia del norte",
            "asia del sur", "asia del este", "asia oriental", "asia central", "siberia", "sudeste asiatico",
            "sudeste de asia", "asia sudoriental", "asia occidental", "asia del oeste", "asia del suroeste",
            "central asia", "east asia", "eastern asia", "north asia", "northern asia", "south asia",
            "southern asia", "southeast asia", "southeastern asia", "western asia", "west asia",
            "southwestern asia", "southwest asia", "commonwealth of nations", "commonwealth",
            "british commonwealth of nations", "african union", "african caribbean and pacific group of states",
            "african economic community", "economic community of west african states",
            "economic community of west african states", "monetary and economic community of central africa",
            "economic community of central african states", "southern african development community",
            "the east african community", "arab maghreb union", "southern african customs union",
            "west african economic and monetary union", "east african community", "east african community",
            "common market for eastern and southern africa", "community of sahel saharan states",
            "the indian ocean commission", "andean community of nations", "caribbean community",
            "organization of american states", "organisation of eastern caribbean state",
            "union of south american nations", "commonwealth of independent states", "russian commonwealth",
            "eurasian economic community", "eurasian economic community",
            "south asian association for regional cooperation", "association of southeast asian nations",
            "pacific islands forum", "asia pacific economic cooperation", "gulf cooperation council",
            "arab league", "european union", "european free trade association", "eurozone", "eurasia", "euroasia",
            "asia pacific", "middle east", "mancomunidad de naciones", "union africana",
            "estados de africa del caribe y del pacifico", "comunidad economica africana",
            "comunidad economica de estados de africa occidental", "zona monetaria de africa occidental",
            "comunidad economica y monetaria de africa central",
            "comunidad economica de los estados de africa central", "comunidad de desarrollo de africa austral",
            "comunidad africana oriental", "union del magreb arabe", "union aduanera de africa austral",
            "union economica y monetaria de africa occidental", "comunidad africana oriental",
            "comunidad de africa del este", "mercado comun de africa oriental y austral",
            "comunidad de los estados sahel saharianos", "comision del oceano indico",
            "comunidad andina de naciones", "comunidad del caribe", "federacion iberoamericana de ombudsman",
            "mercosur", "mercado comun del sur", "organizacion de los estados americanos",
            "organization of eastern caribbean states", "union de naciones suramericanas",
            "comunidad de estados independientes", "comunidad economica eurasiatica",
            "comunidad economica eurasiatica", "asociacion sudasiatica para la cooperacion regional",
            "asociacion de naciones del sudeste asiatico", "foro de las islas del pacifico",
            "cooperacion económica asia pacifico", "consejo de cooperacion para los estados arabes del golfo",
            "liga de estados arabes", "liga arabe", "union europea", "asociacion europea de libre cambio",
            "eurozona", "eurasia", "euroasia", "asia pacífico", "oriente medio", "oriente proximo", "sudamerica",
            "norteamerica", "cono sur", "centroamerica", "paises escandinavos", "scandinavia", "peninsula iberica",
            "iberian peninsula", "mediterraneo", "mediterranean", "andean countries", "paises andinos",
            "sudafrica", "peninsula arabiga", "arabian peninsula", "estados arabes", "arab states", "federacion rusa",
            "paises bajos", "holanda", "reino unido", "inglaterra", "escocia", "gales", "irlanda del norte",
            "gran bretana", "united kingdom", "catalunya", "pais vasco", "euskadi", "comunitat valenciana",
            "gerona", "girona", "la coruna", "a coruna", "lerida", "lleida", "islas baleares", "baleares",
            "marruecos", "magreb", "estados unidos", "united states", "principado de asturias",
            "comunidad foral de navarra", "benelux", "new mexico", "andalusian", "aragonese", "asturian", "cantabrian", "leonese",
            "castillian", "catalan", "madrilenian", "valencian", "extremaduran", "galician", "balearic", "canarian", "riojan",
            "navarrese", "basque", "murcian", "ceutan", "melillan", "castillian", "madrilene",
            "andalusian", "andalusia", "aragonese", "aragon",
            "asturian", "asturias",
            "cantabrian", "cantabria",
            "leonese", "castile and leon",
            "castillian", "castile la mancha",
            "catalan", "catalonia",
            "madrilenian", "madrid",
            "madrilene", "madrid",
            "valencian", "valencian community",
            "extremadura", "extremaduran",
            "galician", "galicia",
            "balearic", "balearic islands",
            "canarian", "canary islands",
            "riojan", "la rioja",
            "navarrese", "navarre",
            "basque", "basque country",
            "murcian", "murcia",
            "ceutan", "ceuta",
            "melillan", "melilla",
            "andaluz", "andalucia",
            "aragones", "aragon",
            "asturiano", "asturias",
            "cantabro", "cantabria",
            "castellano y leones", "castilla y leon",
            "castellano manchego", "castilla la mancha",
            "catalan", "cataluna",
            "madrileno", "comunidad de madrid",
            "valenciano", "comunidad valenciana",
            "extremeno", "extremadura",
            "gallego", "galicia",
            "balear", "islas baleares",
            "canario", "canarias",
            "riojano", "la rioja",
            "navarro", "navarra",
            "vasco", "pais vasco",
            "murciano", "region de murcia",
            "ceuti", "ceuta",
            "melillense", "melilla",
            "andaluza", "andalucia",
            "aragonesa", "aragon",
            "asturiana", "asturias",
            "cantabra", "cantabria",
            "castellano y leonesa", "castilla y leon",
            "castellano manchega", "castilla la mancha",
            "catalana", "cataluna",
            "madrilena", "comunidad de madrid",
            "valenciana", "comunidad valenciana",
            "extremena", "extremadura",
            "gallega", "galicia",
            "baleares", "islas baleares",
            "canaria", "canarias",
            "riojana", "la rioja",
            "navarra", "navarra",
            "vasca", "pais vasco",
            "murciana", "region de murcia",
            "ceuties", "ceuta",
            "melillenses", "melilla",
            "andaluces", "andalucia",
            "aragoneses", "aragon",
            "asturianos", "asturias",
            "cantabros", "cantabria",
            "castellano y leoneses", "castilla y leon",
            "castellano manchegos", "castilla la mancha",
            "catalanes", "cataluna",
            "madrilenos", "comunidad de madrid",
            "valencianos", "comunidad valenciana",
            "extremenos", "extremadura",
            "gallegos", "galicia",
            "balearico", "islas baleares",
            "canarios", "canarias",
            "riojanos", "la rioja",
            "navarros", "navarra",
            "vascos", "pais vasco",
            "murcianos", "region de murcia",
            "andaluzas", "andalucia",
            "aragonesas", "aragon",
            "asturianas", "asturias",
            "cantabras", "cantabria",
            "castellano y leonesas", "castilla y leon",
            "castellano manchegas", "castilla la mancha",
            "catalanas", "cataluna",
            "madrilenas", "comunidad de madrid",
            "valencianas", "comunidad valenciana",
            "extremenas", "extremadura",
            "gallegas", "galicia",
            "balearica", "islas baleares",
            "canarias", "canarias",
            "riojanas", "la rioja",
            "navarras", "navarra",
            "vascas", "pais vasco",
            "murcianas", "region de murcia",
            "montanes", "cantabria",
            "castellano leones", "castilla y leon",
            "castellanomanchego", "castilla la mancha",
            "balearicos", "islas baleares",
            "castellano leonesa", "castilla y leon",
            "montanesa", "cantabria",
            "castellanomanchega", "castilla la mancha",
            "balearicas", "islas baleares",
            "alabamiano", "alabama",
            "alasqueno", "alaska",
            "arizono", "arizona",
            "arkansino", "arkansas",
            "californiano", "california",
            "norcarolino", "carolina de norte",
            "surcarolino", "carolina del sur",
            "coloradino", "colorado",
            "conectiques", "connecticut",
            "dakotano", "dakota del norte",
            "dakotano", "dakota del sur",
            "delawareno", "delaware",
            "floridano", "florida",
            "georgiano", "georgia",
            "hawaiano", "hawai",
            "idahoano", "idaho",
            "ilinoiseno", "illinois",
            "indianio", "indiana",
            "iowense", "iowa",
            "kansano", "kansas",
            "kentuckiano", "kentucky",
            "luisiano", "luisiana",
            "maines", "maine",
            "marilandes", "maryland",
            "massachusetense", "massachussets",
            "michigano", "michigan",
            "minnesotano", "minnesota",
            "misisipiano", "misisipi",
            "misuriano", "misuri",
            "montanes", "montana",
            "nebrasqueno", "nebraska",
            "nevadeno", "nevada",
            "neohampshireno", "nuevo hampshire",
            "neojerseita", "nueva jersey",
            "neomexicano", "nuevo mexico",
            "neoyorquino", "nueva york",
            "ohioano", "ohio",
            "oklahomes", "oklahoma",
            "oregoniano", "oregon",
            "pensilvano", "pensilvania",
            "rodislandes", "rhode island",
            "tennesiano", "tennessee",
            "texano", "texas",
            "utaheno", "utah",
            "vermontes", "vermont",
            "virginiano", "virginia",
            "virginiano", "virginia occidental",
            "washingtoniano", "washington",
            "wisconsinita", "wisconsin",
            "wyominguita", "wyoming",
            "alabamiana", "alabama",
            "alasquena", "alaska",
            "arizona", "arizona",
            "arkansina", "arkansas",
            "californiana", "california",
            "norcarolina", "carolina de norte",
            "surcarolina", "carolina del sur",
            "coloradina", "colorado",
            "conectiquesa", "connecticut",
            "dakotana", "dakota del norte",
            "dakotana", "dakota del sur",
            "delawarena", "delaware",
            "floridana", "florida",
            "georgiana", "georgia",
            "hawaiana", "hawai",
            "idahoana", "idaho",
            "ilinoisena", "illinois",
            "indiania", "indiana",
            "iowenses", "iowa",
            "kansana", "kansas",
            "kentuckiana", "kentucky",
            "luisiana", "luisiana",
            "mainesa", "maine",
            "marilandesa", "maryland",
            "massachusetenses", "massachussets",
            "michigana", "michigan",
            "minnesotana", "minnesota",
            "misisipiana", "misisipi",
            "misurianas", "misuri",
            "montanesa", "montana",
            "nebrasquena", "nebraska",
            "nevadena", "nevada",
            "neohampshirena", "nuevo hampshire",
            "neojerseítas", "nueva jersey",
            "neomexicana", "nuevo mexico",
            "neoyorquina", "nueva york",
            "ohioana", "ohio",
            "oklahomesa", "oklahoma",
            "oregoniana", "oregon",
            "pensilvana", "pensilvania",
            "rodislandesa", "rhode island",
            "tennesiana", "tennessee",
            "texana", "texas",
            "utahena", "utah",
            "vermontesa", "vermont",
            "virginiana", "virginia",
            "virginiana", "virginia occidental",
            "washingtoniana", "washington",
            "wisconsinitas", "wisconsin",
            "wyominguitas", "wyoming",
            "alabamianos", "alabama",
            "alasquenos", "alaska",
            "arizonos", "arizona",
            "arkansinos", "arkansas",
            "californianos", "california",
            "norcarolinos", "carolina de norte",
            "surcarolinos", "carolina del sur",
            "coloradinos", "colorado",
            "conectiqueses", "connecticut",
            "dakotanos", "dakota del norte",
            "dakotanos", "dakota del sur",
            "delawarenos", "delaware",
            "floridanos", "florida",
            "georgianos", "georgia",
            "hawaianos", "hawai",
            "idahoanos", "idaho",
            "ilinoisenos", "illinois",
            "indianios", "indiana",
            "iowes", "iowa",
            "kansanos", "kansas",
            "kentuckianos", "kentucky",
            "luisinos", "luisiana",
            "maineses", "maine",
            "marilandeses", "maryland",
            "massachuseteano", "massachussets",
            "michiganos", "michigan",
            "minnesotanos", "minnesota",
            "misisipianos", "misisipi",
            "misurianos", "misuri",
            "montaneses", "montana",
            "nebrasquenos", "nebraska",
            "nevadenos", "nevada",
            "neohampshirenos", "nuevo hampshire",
            "neomexicanos", "nuevo mexico",
            "neoyorquinos", "nueva york",
            "ohioanos", "ohio",
            "oklahomeses", "oklahoma",
            "oregonianos", "oregon",
            "pensilvanos", "pensilvania",
            "rodislandeses", "rhode island",
            "tennesianos", "tennessee",
            "texanos", "texas",
            "utahenos", "utah",
            "vermonteses", "vermont",
            "virginianos", "virginia",
            "virginianos", "virginia occidental",
            "washingtonianos", "washington",
            "alabamianas", "alabama",
            "alasquenas", "alaska",
            "arizonas", "arizona",
            "arkansinas", "arkansas",
            "californianos", "california",
            "norcarolinas", "carolina de norte",
            "surcarolinas", "carolina del sur",
            "coloradinas", "colorado",
            "conectiquesas", "connecticut",
            "dakotanas", "dakota del norte",
            "dakotanas", "dakota del sur",
            "delawarenas", "delaware",
            "floridanas", "florida",
            "georgianas", "georgia",
            "hawaianas", "hawai",
            "idahoanas", "idaho",
            "ilinoisenas", "illinois",
            "indianias", "indiana",
            "iowesa", "iowa",
            "kansanas", "kansas",
            "kentuckianas", "kentucky",
            "luisinas", "luisiana",
            "mainesas", "maine",
            "marilandesas", "maryland",
            "massachuseteana", "massachussets",
            "michiganas", "michigan",
            "minnesotanas", "minnesota",
            "misisipianas", "misisipi",
            "misurianas", "misuri",
            "montanesas", "montana",
            "nebrasquenas", "nebraska",
            "nevadenas", "nevada",
            "neohampshirenas", "nuevo hampshire",
            "neomexicanas", "nuevo mexico",
            "neoyorquinas", "nueva york",
            "ohioanas", "ohio",
            "oklahomesas", "oklahoma",
            "oregonianas", "oregon",
            "pensilvanas", "pensilvania",
            "rodislandesas", "rhode island",
            "tennesianas", "tennessee",
            "texanas", "texas",
            "utahenas", "utah",
            "vermontesas", "vermont",
            "virginianos", "virginia",
            "virginianos", "virginia occidental",
            "washingtonianas", "washington",
            "afgano",
            "albanes",
            "aleman",
            "andorrano",
            "angoleno",
            "antiguano",
            "saudi",
            "argelino",
            "argentino",
            "armenio",
            "australiano",
            "austriaco",
            "azerbaiyano",
            "bahameno",
            "bangladesi",
            "barbadense",
            "bareini",
            "belga",
            "beliceno",
            "benines",
            "bielorruso",
            "birmano",
            "boliviano",
            "bosnio",
            "botsuano",
            "brasileno",
            "bruneano",
            "bulgaro",
            "burkines",
            "burundes",
            "butanes",
            "caboverdiano",
            "camboyano",
            "camerunes",
            "canadiense",
            "catari",
            "chadiano",
            "chileno",
            "chino",
            "chipriota",
            "vaticano",
            "colombiano",
            "comorense",
            "norcoreano",
            "surcoreano",
            "marfileno",
            "costarricense",
            "croata",
            "cubano",
            "danes",
            "dominiques",
            "ecuatoriano",
            "egipcio",
            "salvadoreno",
            "emirati",
            "eritreo",
            "eslovaco",
            "esloveno",
            "espanol",
            "estadounidense",
            "estonio",
            "etiope",
            "filipino",
            "finlandes",
            "fiyiano",
            "frances",
            "gabones",
            "gambiano",
            "georgiano",
            "ghanes",
            "granadino",
            "griego",
            "guatemalteco",
            "ecuatoguineano",
            "guineano",
            "guineano",
            "guyanes",
            "haitiano",
            "hondureno",
            "hungaro",
            "indio",
            "indonesio",
            "iraqui",
            "irani",
            "irlandes",
            "islandes",
            "marshales",
            "salomonense",
            "israeli",
            "italiano",
            "jamaicano",
            "japones",
            "jordano",
            "kazajo",
            "keniano",
            "kirguis",
            "kiribatiano",
            "kuwaiti",
            "laosiano",
            "lesotense",
            "leton",
            "libanes",
            "liberiano",
            "libio",
            "liechtensteiniano",
            "lituano",
            "luxemburgues",
            "malgache",
            "malasio",
            "malaui",
            "maldivo",
            "maliense",
            "maltes",
            "marroqui",
            "mauriciano",
            "mauritano",
            "mexicano",
            "micronesio",
            "moldavo",
            "monegasco",
            "mongol",
            "montenegrino",
            "mozambiqueno",
            "namibio",
            "nauruano",
            "nepales",
            "nicaraguense",
            "nigerino",
            "nigeriano ",
            "noruego",
            "neozelandes",
            "omani",
            "neerlandes",
            "pakistani",
            "palauano",
            "panameno",
            "papu",
            "paraguayo",
            "peruano",
            "polaco",
            "portugues",
            "britanico",
            "ingles",
            "escoces",
            "gales",
            "centroafricano",
            "checo",
            "macedonio",
            "congoleno",
            "congoleno",
            "dominicano",
            "sudafricano",
            "ruandes",
            "rumano",
            "ruso",
            "samoano",
            "cristobaleno",
            "sanmarinense",
            "sanvicentino",
            "santalucense",
            "santotomense",
            "senegales",
            "serbio",
            "seychellense",
            "sierraleones",
            "singapurense",
            "sirio",
            "somali",
            "ceilanas",
            "suazi",
            "sursudanes",
            "sudanes",
            "sueco",
            "suizo",
            "surinames",
            "tailandes",
            "tanzano",
            "tayiko",
            "timorense",
            "togoles",
            "tongano",
            "trinitense",
            "tunecino",
            "turcomano",
            "turco",
            "tuvaluano",
            "ucraniano",
            "ugandes",
            "uruguayo",
            "uzbeko",
            "vanuatuense",
            "venezolano",
            "vietnamita",
            "yemeni",
            "yibutiano",
            "zambiano",
            "zimbabuense",
            "afgana",
            "albanesa",
            "alemana",
            "andorana",
            "angolena",
            "antiguana",
            "saudita",
            "argelina",
            "argentina",
            "armenia",
            "australiana",
            "austriaca",
            "azerbaiyana",
            "bahamena",
            "bangladesies",
            "barbadenses",
            "bareinies",
            "belgas",
            "belicena",
            "beinesa",
            "bielorrusa",
            "birmana",
            "boliviana",
            "bosnia",
            "botsuana",
            "brasilena",
            "bruneana",
            "bulgara",
            "burkinesa",
            "burundesa",
            "butanesa",
            "caboverdiana",
            "camboyana",
            "camerunesa",
            "canadienses",
            "cataris",
            "chadiana",
            "chilena",
            "china",
            "chipriotas",
            "vaticana",
            "colombiana",
            "comorenses",
            "norcoreana",
            "surcoreana",
            "marfilena",
            "costarricenses",
            "croatas",
            "cubana",
            "danesa",
            "dominiquesa",
            "ecuatoriana",
            "egipcia",
            "salvadorena",
            "emiratis",
            "eritrea",
            "eslovaca",
            "eslovena",
            "espanola",
            "estadounidenses",
            "estonia",
            "filipina",
            "finlandesa",
            "fiyiana",
            "francesa",
            "gabonesa",
            "gambiana",
            "georgiana",
            "ghanesa",
            "granadina",
            "griega",
            "guatemalteca",
            "ecuatoguineana",
            "guineana",
            "guineana",
            "guyanesa",
            "haitiana",
            "hondurena",
            "hungara",
            "india",
            "indonesia",
            "iraquis",
            "iranis",
            "irlandesa",
            "islandesa",
            "marshalesa",
            "salomonenses",
            "israelis",
            "italiana",
            "jamaicana",
            "japonesa",
            "jordana",
            "kazaja",
            "keniana",
            "kirguiso",
            "kiribatiana",
            "kuwaitis",
            "laosiana",
            "lesotenses",
            "letona",
            "libanesa",
            "liberiana",
            "libia",
            "liechtensteiniana",
            "lituana",
            "luxemgurguesa",
            "malgaches",
            "malasia",
            "malauis",
            "maldiva",
            "mali",
            "maltesa",
            "marroquis",
            "mauriciana",
            "mauritana",
            "mexicana",
            "micronesia",
            "moldava",
            "monegasca",
            "mongola",
            "montenegrina",
            "mozambiquena",
            "namibia",
            "nauruana",
            "nepalesa",
            "nicaragüenses",
            "nigerina",
            "nigeriana",
            "noruega",
            "neozelandesa",
            "omanis",
            "neerlandesa",
            "pakistanis",
            "palauana",
            "panamena",
            "papus",
            "paraguaya",
            "peruana",
            "polaca",
            "portuguesa",
            "britanica",
            "inglesa",
            "escocesa",
            "galesa",
            "centroafricana",
            "checa",
            "macedonia",
            "congolenas",
            "congolenas",
            "dominicana",
            "sudafricana",
            "ruandesa",
            "rumana",
            "rusa",
            "samoana",
            "cristobalena",
            "sanmarinenses",
            "sanvicentina",
            "santalucenses",
            "santotomenses",
            "senegalesa",
            "serbia",
            "seychellenses",
            "sierraleonesa",
            "singapurenses",
            "siria",
            "somalis",
            "cielanesa",
            "suazis",
            "sursudanesa",
            "sudanesa",
            "sueca",
            "suiza",
            "surinamesa",
            "tailandesa",
            "tanzana",
            "tayika",
            "timorenses",
            "togolesa",
            "tongano",
            "trinitenses",
            "tunecina",
            "turcomana",
            "turca",
            "tuvaluana",
            "ucraniana",
            "ugandesa",
            "uruguaya",
            "uzbeka",
            "vanuatuenses",
            "venezolana",
            "vietnamitas",
            "yemenis",
            "yibutiana",
            "zambiana",
            "zimbabuenses",};

        String[] engCountries = {"afghanistan", "aland islands", "albania", "algeria", "american samoa", "andorra",
            "angola", "anguilla", "antarctica", "antigua and barbuda", "argentina", "armenia", "aruba", "australia",
            "austria", "azerbaijan", "bahamas", "bahrain", "bangladesh", "barbados", "belarus", "belgium", "belize",
            "benin", "bermuda", "bhutan", "bolivia", "bonaire", "bosnia and herzegovina", "botswana",
            "bouvet island", "brazil", "british indian ocean territory", "brunei darussalam", "bulgaria",
            "burkina faso", "burundi", "cambodia", "cameroon", "canada", "cape verde", "cayman islands",
            "central african republic", "chad", "chile", "china", "christmas island", "cocos islands", "colombia",
            "comoros", "congo", "the democratic republic of the congo", "cook islands", "costa rica",
            "cote d ivoire", "croatia", "cuba", "curacao", "cyprus", "czech republic", "denmark", "djibouti",
            "dominica", "dominican republic", "ecuador", "egypt", "el salvador", "equatorial guinea", "eritrea",
            "estonia", "ethiopia", "falkland islands", "malvinas", "faroe islands", "fiji", "finland", "france",
            "french guiana", "french polynesia", "french southern territories", "gabon", "gambia", "georgia",
            "germany", "ghana", "gibraltar", "greece", "greenland", "grenada", "guadeloupe", "guam", "guatemala",
            "guernsey", "guinea", "guinea bissau", "guyana", "haiti", "heard island and mcdonald islands",
            "holy see", "vatican city state", "honduras", "hong kong", "hungary", "iceland", "india", "indonesia",
            "iran", "iraq", "ireland", "isle of man", "israel", "italy", "jamaica", "japan", "jersey", "jordan",
            "kazakhstan", "kenya", "kiribati", "korea", "republic of korea", "kuwait", "kyrgyzstan",
            "lao people s democratic republic", "latvia", "lebanon", "lesotho", "liberia", "libyan arab jamahiriya",
            "liechtenstein", "lithuania", "luxembourg", "macao", "macedonia", "madagascar", "malawi", "malaysia",
            "maldives", "mali", "malta", "marshall islands", "martinique", "mauritania", "mauritius", "mayotte",
            "mexico", "micronesia", "moldova", "monaco", "mongolia", "montenegro", "montserrat", "morocco",
            "mozambique", "myanmar", "namibia", "nauru", "nepal", "netherlands", "new caledonia", "new zealand",
            "nicaragua", "niger", "nigeria", "niue", "norfolk island", "northern mariana islands", "norway",
            "occupied Palestinian territory", "oman", "pakistan", "palau", "panama", "papua new guinea", "paraguay",
            "peru", "philippines", "pitcairn", "poland", "portugal", "puerto rico", "qatar", "reunion island", "romania",
            "russian federation", "rwanda", "saint barthelemy", "saint helena", "saint kitts and nevis",
            "saint lucia", "saint martin", "saint pierre and miquelon", "saint vincent and The grenadines", "samoa",
            "san marino", "sao tome and principe", "saudi arabia", "senegal", "serbia", "seychelles",
            "sierra leone", "singapore", "sint maarten", "slovakia", "slovenia", "solomon islands", "somalia",
            "south africa", "south georgia and the south sandwich islands", "south sudan", "spain", "sri lanka",
            "sudan", "suriname", "svalbard and jan mayen", "swaziland", "sweden", "switzerland",
            "syrian arab republic", "taiwan", "tajikistan", "tanzania", "thailand", "timor leste", "togo",
            "tokelau", "tonga", "trinidad and tobago", "tunisia", "turkey", "turkmenistan",
            "turks and caicos islands", "tuvalu", "uganda", "ukraine", "united arab emirates", "united kingdom",
            "united states", "united states of america", "united states minor outlying islands", "uruguay", "uzbekistan", "vanuatu",
            "venezuela", "viet nam", "virgin islands", "virgin islands", "wallis and futuna", "western sahara",
            "yemen", "zambia", "zimbabwe",};

        String[][] spaCountriesCapitals = {
            {"albania", "tirana"},
            {"alemania", " berlin"},
            {"andorra", "andorra la vieja"},
            {"armenia", "erevan"},
            {"austria", "viena"},
            {"azerbaiyán", "baku"},
            {"belgica", "bruselas"},
            {"bielorrusia", "minsk"},
            {"bosnia y herzegovina", "sarajevo"},
            {"bulgaria", "sofia"},
            {"republica checa", "praga"},
            {"croacia", "zagreb"},
            {"dinamarca", "copenhague"},
            {"eslovaquia", "bratislava"},
            {"eslovenia", "lublijana"},
            {"espana", "madrid"},
            {"estonia", "tallin"},
            {"finlandia", "helsinki"},
            {"francia", "paris"},
            {"georgia", "tiflis"},
            {"grecia", "atenas"},
            {"hungria", "budapest"},
            {"irlanda", "dublin"},
            {"islandia", "reikiavik"},
            {"italia", "roma"},
            {"letonia", "riga"},
            {"liechtenstein", "vaduz"},
            {"lituania", "vilna"},
            {"luxemburgo", "luxemburgo"},
            {"republica de macedonia", "skopje"},
            {"malta", "la valeta"},
            {"moldavia", "chisinau"},
            {"monaco", "monaco"},
            {"montenegro", "podgorica"},
            {"noruega", "oslo"},
            {"paises bajos", "amsterdam"},
            {"polonia", "varsovia"},
            {"portugal", "lisboa"},
            {"reino unido", "londres"},
            {"rumania", "bucarest"},
            {"rusia", "moscu"},
            {"san marino", "ciudad de san marino"},
            {"serbia", "belgrado"},
            {"suecia", "estocolmo"},
            {"suiza", "berna"},
            {"ucrania", "kiev"},
            {"vaticano", "ciudad del vaticano"},
            {"antigua y barbuda", "saint john s"},
            {"argentina", "buenos aires"},
            {"bahamas", "nassau"},
            {"barbados", "bridgetown"},
            {"belice", "belmopan"},
            {"bolivia", "sucre"},
            {"brasil", "brasilia"},
            {"canada", "ottawa"},
            {"chile", "santiago de chile"},
            {"colombia", "bogota"},
            {"costa rica", "san jose"},
            {"cuba", "la habana"},
            {"dominica", "roseau"},
            {"republica dominicana", "santo domingo"},
            {"ecuador", "quito"},
            {"el salvador", "san salvador"},
            {"estados unidos", "washington d.c."},
            {"estados unidos", "washington d. c."},
            {"estados unidos", "washington dc"},
            {"granada", "saint george s"},
            {"guatemala", "ciudad de guatemala"},
            {"guyana", "georgetown"},
            {"haiti", "puerto principe"},
            {"honduras", "tegucigalpa"},
            {"jamaica", "kingston"},
            {"mexico", "mexico d.f."},
            {"mexico", "mexico d. f."},
            {"mexico", "mexico df"},
            {"nicaragua", "managua"},
            {"panama", "ciudad de panama"},
            {"paraguay", "asuncion"},
            {"peru", "lima"},
            {"puerto rico", "san juan"},
            {"san cristobal y nieves", "basseterre"},
            {"santa lucia", "castries"},
            {"san vicente y las granadinas", "kingstown"},
            {"surinam", "paramaribo"},
            {"trinidad y tobago", "puerto espana"},
            {"uruguay", "montevideo"},
            {"venezuela", "caracas"},
            {"afganistan", "kabul"},
            {"arabia saudita", "riad"},
            {"barein", "manama"},
            {"banglades", "daca"},
            {"brunei", "bandar seri begawan"},
            {"butan", "timbu"},
            {"camboya", "pnon pehn"},
            {"catar", "doha"},
            {"china", "pekin"},
            {"chipre", "nicosia"},
            {"corea del norte", "pyongyang"},
            {"corea del sur", "seul"},
            {"emiratos arabes unidos", "abu dabi"},
            {"filipinas", "manila"},
            {"india", "nueva delhi"},
            {"indonesia", "yakarta"},
            {"iran", "teheran"},
            {"iraq", "bagdad"},
            {"israel", "jerusalen"},
            {"japon", "tokio"},
            {"jordania", "amman"},
            {"kazajistan", "astana"},
            {"kirguistan", "biskek"},
            {"kuwait", "ciudad de kuwait"},
            {"laos", "vientian"},
            {"líbano", "beirut"},
            {"malasia", "kuala lumpur"},
            {"maldivas", "male"},
            {"mongolia", "ulan bator"},
            {"myanmar", "naypyidaw"},
            {"nepal", "katmandu"},
            {"oman", "mascate"},
            {"pakistan", "islamabad"},
            {"palestinas", "ramala"},
            {"singapur", "singapur"},
            {"siria", "damasco"},
            {"sri lanka", "colombo"},
            {"tailandia", "bangkok"},
            {"taiwan", "taipeh"},
            {"tayikistan", "dusambe"},
            {"timor oriental", "dili"},
            {"turkmenistan", "asjabad"},
            {"turquia", "ankara"},
            {"uzbekistan", "tashkent"},
            {"vietnam", "hanoi"},
            {"yemen", "sana"},
            {"angola", "luanda"},
            {"argelia", "argel"},
            {"benin", "porto novo"},
            {"botsuana", "gaberones"},
            {"burkina faso", "uagadugu"},
            {"burundi", "buyumbura"},
            {"cabo verde", "praia"},
            {"camerun", "yaunde"},
            {"republica centroafricana", "bangui"},
            {"chad", "yamena"},
            {"Comoras", "moroni"},
            {"republica del Congo", "brazzaville"},
            {"republica democratica del congo", "kinshasa"},
            {"costa de marfil", "yamusukro"},
            {"costa de marfil", "abiyan"},
            {"egipto", "el cairo"},
            {"eritrea", "asmara"},
            {"etiopia", "adis abeba"},
            {"gabon", "libreville"},
            {"gambia", "banjul"},
            {"ghana", "accra"},
            {"guinea", "conakry"},
            {"guinea bissau", "bissau"},
            {"guinea ecuatorial", "malabo"},
            {"kenia", "nairobi"},
            {"lesoto", "maseru"},
            {"liberia", "monrovia"},
            {"libia", "tripoli"},
            {"madagascar", "antananarivo"},
            {"malaui", "lilongue"},
            {"mali", "bamako"},
            {"marruecos", "rabat"},
            {"mauricio", "port louis"},
            {"mauritania", "nuakchot"},
            {"mozambique", "maputo"},
            {"namibia", "windhoek"},
            {"niger", "niamey"},
            {"nigeria", "abuya"},
            {"republica saharaui", "el aaiun"},
            {"ruanda", "kigali"},
            {"santo tome y principe", "santo tome"},
            {"senegal", "dakar"},
            {"seychelles", "victoria"},
            {"sierra leona", "freetown"},
            {"somalia", "mogadiscio"},
            {"suazilandia", "mbabane"},
            {"sudafrica	pretoria", "ciudad del cabo"},
            {"sudafrica pretoria", "bloemfontein"},
            {"sudán del norte", "jartum"},
            {"sudan del sur", "yuba"},
            {"tanzania", "dodoma"},
            {"togo", "lome"},
            {"tunez", "tunez"},
            {"uganda", "kampala"},
            {"yibuti", "yibuti"},
            {"zambia", "lusaka"},
            {"zimbabue", "harare"},
            {"australia", "canberra"},
            {"fiyi", "suva"},
            {"kiribati", "tarawa"},
            {"islas marshall", "majuro"},
            {"micronesia", "palikir"},
            {"nauru", "yaren"},
            {"nueva zelanda", "wellington"},
            {"palaos", "melekeok"},
            {"papua nueva guinea", "port moresby"},
            {"islas salomon", "honiara"},
            {"samoa", "apia"},
            {"tonga", "nuku alofa"},
            {"tuvalu", "funafuti"},
            {"vanuatu", "port vila"}
        };

        String[][] engCountriesCapitals = {
            {"afghanistan", "kabul"},
            {"albania", "tirana"},
            {"algeria", "algiers"},
            {"andorra", "andorra la vella"},
            {"angola", "luanda"},
            {"antigua and barbuda", "saint john s"},
            {"argentina", "buenos aires"},
            {"armenia", "yerevan"},
            {"australia", "canberra"},
            {"austria", "vienna"},
            {"azerbaijan", "baku"},
            {"the bahamas", "nassau"},
            {"bahrain", "manama"},
            {"bangladesh", "dhaka"},
            {"barbados", "bridgetown"},
            {"belarus", "minsk"},
            {"belgium", "brussels"},
            {"belize", "belmopan"},
            {"benin", "porto novo"},
            {"bhutan", "thimphu"},
            {"bolivia", "sucre"},
            {"bosnia and herzegovina", "sarajevo"},
            {"botswana", "gaborone"},
            {"brazil", "brasilia"},
            {"brunei", "bandar seri begawan"},
            {"bulgaria", "sofia"},
            {"burkina faso", "ouagadougou"},
            {"burundi", "bujumbura"},
            {"cambodia", "phnom penh"},
            {"cameroon", "yaounde"},
            {"canada", "ottawa"},
            {"cape verde", "praia"},
            {"central african republic", "bangui"},
            {"chad", "n djamena"},
            {"chile", "santiago"},
            {"china", "beijing"},
            {"colombia", "bogota"},
            {"comoros", "moroni"},
            {"congo", "brazzaville"},
            {"congo", "democratic republic of the kinshasa"},
            {"costa rica", "san jose"},
            {"cote d ivoire", "yamoussoukro"},
            {"croatia", "zagreb"},
            {"cuba", "havana"},
            {"cyprus", "nicosia"},
            {"czech republic", "prague"},
            {"denmark", "copenhagen"},
            {"djibouti", "djibouti"},
            {"dominica", "roseau"},
            {"dominican republic", "santo domingo"},
            {"east timor", "dili"},
            {"ecuador", "quito"},
            {"egypt", "cairo"},
            {"el salvador", "san salvador"},
            {"equatorial", "guinea malabo"},
            {"eritrea", "asmara"},
            {"estonia", "tallinn"},
            {"ethiopia", "addis ababa"},
            {"fiji", "suva"},
            {"finland", "helsinki"},
            {"france", "paris"},
            {"gabon", "libreville"},
            {"the gambia", "banjul"},
            {"georgia", "tbilisi"},
            {"germany", "berlin"},
            {"greece", "athens"},
            {"grenada", "saint george s"},
            {"guatemala", "guatemala city"},
            {"india", "new delhi"},
            {"italy", "rome"},
            {"japan", "tokyo"},
            {"korea north", "pyongyang"},
            {"korea south", "seoul"},
            {"luxembourg", "luxembourg"},
            {"mexico", "mexico city"},
            {"netherlands", "amsterdam"},
            {"new zealand", "wellington"},
            {"russia", "moscow"},
            {"solomon islands", "honiara"},
            {"south africa", "pretoria"},
            {"swaziland", "mbabane"},
            {"sweden", "stockholm"},
            {"switzerland", "bern"},
            {"syria", "damascus"},
            {"united arab emirates", "abu dhabi"},
            {"united kingdom", "london"},
            {"united states of America", "washington d.c."},
            {"united states", "washington d.c."},
            {"vatican city", "vatican City"}
        };

        String[][] spaAdminDivisionsFirstLevel = {
            {"espana", "andalucia"},
            {"espana", "aragon"},
            {"espana", "principado de asturias"},
            {"espana", "islas baleares"},
            {"espana", "canarias"},
            {"espana", "cantabria"},
            {"espana", "castilla la mancha"},
            {"espana", "castilla y leon"},
            {"espana", "cataluna"},
            {"espana", "comunidad valenciana"},
            {"espana", "extremadura"},
            {"espana", "galicia"},
            {"espana", "la rioja"},
            {"espana", "comunidad de madrid"},
            {"espana", "comunidad foral de navarra"},
            {"espana", "pais vasco"},
            {"espana", "region de murcia"}
        };

        tableSpaAdminDivFirstLevel = spaAdminDivisionsFirstLevel;

        String[][] spaAdminDivisionsSecondLevel = {
            {"andalucia", "almeria"},
            {"andalucia", "cadiz"},
            {"andalucia", "cordoba"},
            {"andalucia", "granada"},
            {"andalucia", "huelva"},
            {"andalucia", "jaen"},
            {"andalucia", "malaga"},
            {"andalucia", "sevilla"},
            {"aragon", "huesca"},
            {"aragon", "teruel"},
            {"aragon", "zaragoza"},
            {"principado de asturias", "asturias"},
            {"principado de asturias", "oviedo"},
            {"islas baleares", "islas baleares"},
            {"islas baleares", "palma de mallorca"},
            {"canarias", "las palmas de gran canaria"},
            {"canarias", "las palmas"},
            {"canarias", "santa cruz de tenerife"},
            {"cantabria", "santander"},
            {"castilla la mancha", "albacete"},
            {"castilla la mancha", "ciudad real"},
            {"castilla la mancha", "cuenca"},
            {"castilla la mancha", "guadalajara"},
            {"castilla la mancha", "toledo"},
            {"castilla y leon", "avila"},
            {"castilla y leon", "burgos"},
            {"castilla y leon", "leon"},
            {"castilla y leon", "palencia"},
            {"castilla y leon", "salamanca"},
            {"castilla y leon", "segovia"},
            {"castilla y leon", "soria"},
            {"castilla y leon", "valladolid"},
            {"castilla y leon", "zamora"},
            {"cataluna", "barcelona"},
            {"cataluna", "gerona"},
            {"cataluna", "lerida"},
            {"cataluna", "tarragona"},
            {"comunidad valenciana", "alicante"},
            {"comunidad valenciana", "castellon"},
            {"comunidad valenciana", "castellon de la plana"},
            {"comunidad valenciana", "valencia"},
            {"extremadura", "badajoz"},
            {"extremadura", "merida"},
            {"extremadura", "caceres"},
            {"galicia", "santiago de compostela"},
            {"galicia", "la coruna"},
            {"galicia", "lugo"},
            {"galicia", "orense"},
            {"galicia", "pontevedra"},
            {"la rioja", "logrono"},
            {"la rioja", "la rioja"},
            {"comunidad de madrid", "madrid"},
            {"comunidad foral de navarra", "pamplona"},
            {"comunidad foral de navarra", "navarra"},
            {"pais vasco", "vitoria"},
            {"pais vasco", "alava"},
            {"pais vasco", "guipuzcoa"},
            {"pais vasco", "san sebastian"},
            {"pais vasco", "vizcaya"},
            {"pais vasco", "bilbao"},
            {"region de murcia", "murcia"},
            {"region de murcia", "region de murcia"},};

        String[][] engAdminDivisionsFirstLevel = {
            {"spain", "andalusia"},
            {"spain", "aragon"},
            {"spain", "asturias"},
            {"spain", "balearic islands"},
            {"spain", "basque country"},
            {"spain", "canary islands"},
            {"spain", "cantabria"},
            {"spain", "castile la mancha"},
            {"spain", "castile and leon"},
            {"spain", "catalonia"},
            {"spain", "extremadura"},
            {"spain", "galicia"},
            {"spain", "ceuta"},
            {"spain", "la rioja"},
            {"spain", "madrid"},
            {"spain", "murcia"},
            {"spain", "navarre"},
            {"spain", "valencian community"},
            {"canada", "ashmore and cartier islands"},
            {"canada", "australian antarctic territory"},
            {"canada", "australian capital territory"},
            {"canada", "christmas island"},
            {"canada", "cocos  islands"},
            {"canada", "keeling islands)"},
            {"canada", "coral sea islands"},
            {"canada", "heard island and mcdonald islands"},
            {"canada", "jervis bay territory"},
            {"canada", "new south wales"},
            {"canada", "norfolk island"},
            {"canada", "northern territory"},
            {"canada", "queensland"},
            {"canada", "south australia"},
            {"canada", "tasmania"},
            {"canada", "victoria"},
            {"canada", "western australia"},};

        tableEngAdminDivFirstLevel = engAdminDivisionsFirstLevel;

        String[][] engAdminDivisionsSecondLevel = {
            {"valencian community", "valencia"},
            {"valencian community", "castellon"},
            {"valencian community", "alicante"},
            {"andalusia", "almeria"},
            {"andalusia", "cadiz"},
            {"andalusia", "cordoba"},
            {"andalusia", "granada"},
            {"andalusia", "huelva"},
            {"andalusia", "jaen"},
            {"andalusia", "malaga"},
            {"andalusia", "sevilla"},
            {"castile la mancha", "albacete"},
            {"castile la mancha", "ciudad real"},
            {"castile la mancha", "cuenca"},
            {"castile la mancha", "guadalajara"},
            {"castile la mancha", "toledo"},
            {"navarre", "pamplona"},
            {"navarre", "navarre"},
            {"balearic islands", "balearic islands"},
            {"balearic islands", "palma de mallorca"},
            {"basque country", "alava"},
            {"basque country", "guipuzcoa"},
            {"basque country", "san sebastian"},
            {"basque country", "vizcaya"},
            {"canary islands", "las palmas de gran canaria"},
            {"canary islands", "las palmas"},
            {"canary islands", "santa cruz de tenerife"},
            {"castile y leon", "avila"},
            {"castile y leon", "burgos"},
            {"castile y leon", "leon"},
            {"castile y leon", "palencia"},
            {"castile y leon", "salamanca"},
            {"castile y leon", "segovia"},
            {"castile y leon", "soria"},
            {"castile y leon", "valladolid"},
            {"castile y leon", "zamora"},};

        String[] acronyms = {"US", "U.S.", "USA", "U.S.A.", "UK", "U.K.", "EUA", "E.U.A", "EEUU", "EE.UU.", "EAC", "AU",
            "NOA", "ACP", "ECOWAS", "CEMAC", "FIO", "OEA", "OECS", "ASEAN", "SAARC", "OSCE", "AU", "ACP", "AEC",
            "ECOWAS", "CEMAC", "ECCAS", "SADC", "EAC", "AMU", "SACU", "UEMOA", "EAC", "EAC", "COMESA", "COI",
            "CARICOM", "OAS", "OECS", "USAN", "CIS", "EAEC", "EURASEC", "SAARC", "ASEAN", "PIF", "APEC", "GCC",
            "EU", "EFTA", "CEA", "CEDEAO", "ZMAO", "CEMAC", "CEEAC", "SADC", "CAO", "UMA", "UAAA", "UEMOA", "CAO",
            "CAE", "COMESA", "CEN SAD", "COI", "OECS", "UNASUR", "CEI", "CEEA", "ASACR", "ANSA", "PIF", "APEC",
            "CCEAG", "UE", "AELC"
        };

        spaCountriesMap = new Hashtable<>(INITIAL_CAPACITY_FOR_CONTRIES);
        spaFirstLevelLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ADMIN_DIV_FL);
        spaSecondLevelLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ADMIN_DIV_SL);
        spaCapitalLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_CONTRIES);
        spaAcronymLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ACRONYMS);

        engCountriesMap = new Hashtable<>(INITIAL_CAPACITY_FOR_CONTRIES);
        engFirstLevelLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ADMIN_DIV_FL);
        engSecondLevelLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ADMIN_DIV_SL);
        engCapitalLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_CONTRIES);
        engAcronymLocationMap = new Hashtable<>(INITIAL_CAPACITY_FOR_ACRONYMS);

        gentilics = new Hashtable<>(1000);

        gentilics.put("andalusian", "andalusia");
        gentilics.put("aragonese", "aragon");
        gentilics.put("asturian", "asturias");
        gentilics.put("cantabrian", "cantabria");
        gentilics.put("leonese", "castile and leon");
        gentilics.put("castillian", "castile la mancha");
        gentilics.put("catalan", "catalonia");
        gentilics.put("madrilenian", "madrid");
        gentilics.put("madrilene", "madrid");
        gentilics.put("valencian", "valencian community");
        gentilics.put("extremadura", "extremaduran");
        gentilics.put("galician", "galicia");
        gentilics.put("balearic", "balearic islands");
        gentilics.put("canarian", "canary islands");
        gentilics.put("riojan", "la rioja");
        gentilics.put("navarrese", "navarre");
        gentilics.put("basque", "basque country");
        gentilics.put("murcian", "murcia");
        gentilics.put("ceutan", "ceuta");
        gentilics.put("melillan", "melilla");
        gentilics.put("andaluz", "andalucia");
        gentilics.put("aragones", "aragon");
        gentilics.put("asturiano", "asturias");
        gentilics.put("cantabro", "cantabria");
        gentilics.put("castellano y leones", "castilla y leon");
        gentilics.put("castellano manchego", "castilla la mancha");
        gentilics.put("catalan", "cataluna");
        gentilics.put("madrileno", "comunidad de madrid");
        gentilics.put("valenciano", "comunidad valenciana");
        gentilics.put("extremeno", "extremadura");
        gentilics.put("gallego", "galicia");
        gentilics.put("balear", "islas baleares");
        gentilics.put("canario", "canarias");
        gentilics.put("riojano", "la rioja");
        gentilics.put("navarro", "navarra");
        gentilics.put("vasco", "pais vasco");
        gentilics.put("murciano", "region de murcia");
        gentilics.put("ceuti", "ceuta");
        gentilics.put("melillense", "melilla");
        gentilics.put("andaluza", "andalucia");
        gentilics.put("aragonesa", "aragon");
        gentilics.put("asturiana", "asturias");
        gentilics.put("cantabra", "cantabria");
        gentilics.put("castellano y leonesa", "castilla y leon");
        gentilics.put("castellano manchega", "castilla la mancha");
        gentilics.put("catalana", "cataluna");
        gentilics.put("madrilena", "comunidad de madrid");
        gentilics.put("valenciana", "comunidad valenciana");
        gentilics.put("extremena", "extremadura");
        gentilics.put("gallega", "galicia");
        gentilics.put("baleares", "islas baleares");
        gentilics.put("canaria", "canarias");
        gentilics.put("riojana", "la rioja");
        gentilics.put("navarra", "navarra");
        gentilics.put("vasca", "pais vasco");
        gentilics.put("murciana", "region de murcia");
        gentilics.put("ceuties", "ceuta");
        gentilics.put("melillenses", "melilla");
        gentilics.put("andaluces", "andalucia");
        gentilics.put("aragoneses", "aragon");
        gentilics.put("asturianos", "asturias");
        gentilics.put("cantabros", "cantabria");
        gentilics.put("castellano y leoneses", "castilla y leon");
        gentilics.put("castellano manchegos", "castilla la mancha");
        gentilics.put("catalanes", "cataluna");
        gentilics.put("madrilenos", "comunidad de madrid");
        gentilics.put("valencianos", "comunidad valenciana");
        gentilics.put("extremenos", "extremadura");
        gentilics.put("gallegos", "galicia");
        gentilics.put("balearico", "islas baleares");
        gentilics.put("canarios", "canarias");
        gentilics.put("riojanos", "la rioja");
        gentilics.put("navarros", "navarra");
        gentilics.put("vascos", "pais vasco");
        gentilics.put("murcianos", "region de murcia");
        gentilics.put("andaluzas", "andalucia");
        gentilics.put("aragonesas", "aragon");
        gentilics.put("asturianas", "asturias");
        gentilics.put("cantabras", "cantabria");
        gentilics.put("castellano y leonesas", "castilla y leon");
        gentilics.put("castellano manchegas", "castilla la mancha");
        gentilics.put("catalanas", "cataluna");
        gentilics.put("madrilenas", "comunidad de madrid");
        gentilics.put("valencianas", "comunidad valenciana");
        gentilics.put("extremenas", "extremadura");
        gentilics.put("gallegas", "galicia");
        gentilics.put("balearica", "islas baleares");
        gentilics.put("canarias", "canarias");
        gentilics.put("riojanas", "la rioja");
        gentilics.put("navarras", "navarra");
        gentilics.put("vascas", "pais vasco");
        gentilics.put("murcianas", "region de murcia");
        gentilics.put("montanes", "cantabria");
        gentilics.put("castellano leones", "castilla y leon");
        gentilics.put("castellanomanchego", "castilla la mancha");
        gentilics.put("balearicos", "islas baleares");
        gentilics.put("castellano leonesa", "castilla y leon");
        gentilics.put("montanesa", "cantabria");
        gentilics.put("castellanomanchega", "castilla la mancha");
        gentilics.put("balearicas", "islas baleares");
        gentilics.put("alabamiano", "alabama");
        gentilics.put("alasqueno", "alaska");
        gentilics.put("arizono", "arizona");
        gentilics.put("arkansino", "arkansas");
        gentilics.put("californiano", "california");
        gentilics.put("norcarolino", "carolina de norte");
        gentilics.put("surcarolino", "carolina del sur");
        gentilics.put("coloradino", "colorado");
        gentilics.put("conectiques", "connecticut");
        gentilics.put("dakotano", "dakota del norte");
        gentilics.put("dakotano", "dakota del sur");
        gentilics.put("delawareno", "delaware");
        gentilics.put("floridano", "florida");
        gentilics.put("georgiano", "georgia");
        gentilics.put("hawaiano", "hawai");
        gentilics.put("idahoano", "idaho");
        gentilics.put("ilinoiseno", "illinois");
        gentilics.put("indianio", "indiana");
        gentilics.put("iowense", "iowa");
        gentilics.put("kansano", "kansas");
        gentilics.put("kentuckiano", "kentucky");
        gentilics.put("luisiano", "luisiana");
        gentilics.put("maines", "maine");
        gentilics.put("marilandes", "maryland");
        gentilics.put("massachusetense", "massachussets");
        gentilics.put("michigano", "michigan");
        gentilics.put("minnesotano", "minnesota");
        gentilics.put("misisipiano", "misisipi");
        gentilics.put("misuriano", "misuri");
        gentilics.put("montanes", "montana");
        gentilics.put("nebrasqueno", "nebraska");
        gentilics.put("nevadeno", "nevada");
        gentilics.put("neohampshireno", "nuevo hampshire");
        gentilics.put("neojerseita", "nueva jersey");
        gentilics.put("neomexicano", "nuevo mexico");
        gentilics.put("neoyorquino", "nueva york");
        gentilics.put("ohioano", "ohio");
        gentilics.put("oklahomes", "oklahoma");
        gentilics.put("oregoniano", "oregon");
        gentilics.put("pensilvano", "pensilvania");
        gentilics.put("rodislandes", "rhode island");
        gentilics.put("tennesiano", "tennessee");
        gentilics.put("texano", "texas");
        gentilics.put("utaheno", "utah");
        gentilics.put("vermontes", "vermont");
        gentilics.put("virginiano", "virginia");
        gentilics.put("virginiano", "virginia occidental");
        gentilics.put("washingtoniano", "washington");
        gentilics.put("wisconsinita", "wisconsin");
        gentilics.put("wyominguita", "wyoming");
        gentilics.put("alabamiana", "alabama");
        gentilics.put("alasquena", "alaska");
        gentilics.put("arizona", "arizona");
        gentilics.put("arkansina", "arkansas");
        gentilics.put("californiana", "california");
        gentilics.put("norcarolina", "carolina de norte");
        gentilics.put("surcarolina", "carolina del sur");
        gentilics.put("coloradina", "colorado");
        gentilics.put("conectiquesa", "connecticut");
        gentilics.put("dakotana", "dakota del norte");
        gentilics.put("dakotana", "dakota del sur");
        gentilics.put("delawarena", "delaware");
        gentilics.put("floridana", "florida");
        gentilics.put("georgiana", "georgia");
        gentilics.put("hawaiana", "hawai");
        gentilics.put("idahoana", "idaho");
        gentilics.put("ilinoisena", "illinois");
        gentilics.put("indiania", "indiana");
        gentilics.put("iowenses", "iowa");
        gentilics.put("kansana", "kansas");
        gentilics.put("kentuckiana", "kentucky");
        gentilics.put("luisiana", "luisiana");
        gentilics.put("mainesa", "maine");
        gentilics.put("marilandesa", "maryland");
        gentilics.put("massachusetenses", "massachussets");
        gentilics.put("michigana", "michigan");
        gentilics.put("minnesotana", "minnesota");
        gentilics.put("misisipiana", "misisipi");
        gentilics.put("misurianas", "misuri");
        gentilics.put("montanesa", "montana");
        gentilics.put("nebrasquena", "nebraska");
        gentilics.put("nevadena", "nevada");
        gentilics.put("neohampshirena", "nuevo hampshire");
        gentilics.put("neojerseítas", "nueva jersey");
        gentilics.put("neomexicana", "nuevo mexico");
        gentilics.put("neoyorquina", "nueva york");
        gentilics.put("ohioana", "ohio");
        gentilics.put("oklahomesa", "oklahoma");
        gentilics.put("oregoniana", "oregon");
        gentilics.put("pensilvana", "pensilvania");
        gentilics.put("rodislandesa", "rhode island");
        gentilics.put("tennesiana", "tennessee");
        gentilics.put("texana", "texas");
        gentilics.put("utahena", "utah");
        gentilics.put("vermontesa", "vermont");
        gentilics.put("virginiana", "virginia");
        gentilics.put("virginiana", "virginia occidental");
        gentilics.put("washingtoniana", "washington");
        gentilics.put("wisconsinitas", "wisconsin");
        gentilics.put("wyominguitas", "wyoming");
        gentilics.put("alabamianos", "alabama");
        gentilics.put("alasquenos", "alaska");
        gentilics.put("arizonos", "arizona");
        gentilics.put("arkansinos", "arkansas");
        gentilics.put("californianos", "california");
        gentilics.put("norcarolinos", "carolina de norte");
        gentilics.put("surcarolinos", "carolina del sur");
        gentilics.put("coloradinos", "colorado");
        gentilics.put("conectiqueses", "connecticut");
        gentilics.put("dakotanos", "dakota del norte");
        gentilics.put("dakotanos", "dakota del sur");
        gentilics.put("delawarenos", "delaware");
        gentilics.put("floridanos", "florida");
        gentilics.put("georgianos", "georgia");
        gentilics.put("hawaianos", "hawai");
        gentilics.put("idahoanos", "idaho");
        gentilics.put("ilinoisenos", "illinois");
        gentilics.put("indianios", "indiana");
        gentilics.put("iowes", "iowa");
        gentilics.put("kansanos", "kansas");
        gentilics.put("kentuckianos", "kentucky");
        gentilics.put("luisinos", "luisiana");
        gentilics.put("maineses", "maine");
        gentilics.put("marilandeses", "maryland");
        gentilics.put("massachuseteano", "massachussets");
        gentilics.put("michiganos", "michigan");
        gentilics.put("minnesotanos", "minnesota");
        gentilics.put("misisipianos", "misisipi");
        gentilics.put("misurianos", "misuri");
        gentilics.put("montaneses", "montana");
        gentilics.put("nebrasquenos", "nebraska");
        gentilics.put("nevadenos", "nevada");
        gentilics.put("neohampshirenos", "nuevo hampshire");
        gentilics.put("neomexicanos", "nuevo mexico");
        gentilics.put("neoyorquinos", "nueva york");
        gentilics.put("ohioanos", "ohio");
        gentilics.put("oklahomeses", "oklahoma");
        gentilics.put("oregonianos", "oregon");
        gentilics.put("pensilvanos", "pensilvania");
        gentilics.put("rodislandeses", "rhode island");
        gentilics.put("tennesianos", "tennessee");
        gentilics.put("texanos", "texas");
        gentilics.put("utahenos", "utah");
        gentilics.put("vermonteses", "vermont");
        gentilics.put("virginianos", "virginia");
        gentilics.put("virginianos", "virginia occidental");
        gentilics.put("washingtonianos", "washington");
        gentilics.put("alabamianas", "alabama");
        gentilics.put("alasquenas", "alaska");
        gentilics.put("arizonas", "arizona");
        gentilics.put("arkansinas", "arkansas");
        gentilics.put("californianos", "california");
        gentilics.put("norcarolinas", "carolina de norte");
        gentilics.put("surcarolinas", "carolina del sur");
        gentilics.put("coloradinas", "colorado");
        gentilics.put("conectiquesas", "connecticut");
        gentilics.put("dakotanas", "dakota del norte");
        gentilics.put("dakotanas", "dakota del sur");
        gentilics.put("delawarenas", "delaware");
        gentilics.put("floridanas", "florida");
        gentilics.put("georgianas", "georgia");
        gentilics.put("hawaianas", "hawai");
        gentilics.put("idahoanas", "idaho");
        gentilics.put("ilinoisenas", "illinois");
        gentilics.put("indianias", "indiana");
        gentilics.put("iowesa", "iowa");
        gentilics.put("kansanas", "kansas");
        gentilics.put("kentuckianas", "kentucky");
        gentilics.put("luisinas", "luisiana");
        gentilics.put("mainesas", "maine");
        gentilics.put("marilandesas", "maryland");
        gentilics.put("massachuseteana", "massachussets");
        gentilics.put("michiganas", "michigan");
        gentilics.put("minnesotanas", "minnesota");
        gentilics.put("misisipianas", "misisipi");
        gentilics.put("misurianas", "misuri");
        gentilics.put("montanesas", "montana");
        gentilics.put("nebrasquenas", "nebraska");
        gentilics.put("nevadenas", "nevada");
        gentilics.put("neohampshirenas", "nuevo hampshire");
        gentilics.put("neomexicanas", "nuevo mexico");
        gentilics.put("neoyorquinas", "nueva york");
        gentilics.put("ohioanas", "ohio");
        gentilics.put("oklahomesas", "oklahoma");
        gentilics.put("oregonianas", "oregon");
        gentilics.put("pensilvanas", "pensilvania");
        gentilics.put("rodislandesas", "rhode island");
        gentilics.put("tennesianas", "tennessee");
        gentilics.put("texanas", "texas");
        gentilics.put("utahenas", "utah");
        gentilics.put("vermontesas", "vermont");
        gentilics.put("virginianos", "virginia");
        gentilics.put("virginianos", "virginia occidental");
        gentilics.put("washingtonianas", "washington");
        gentilics.put("alabamian", "alabama");
        gentilics.put("alaskan", "alaska");
        gentilics.put("arizonan", "arizona");
        gentilics.put("arkansan", "arkansas");
        gentilics.put("californian", "california");
        gentilics.put("coloradan", "colorado");
        gentilics.put("connecticutian", "connecticut");
        gentilics.put("delawarean", "delaware");
        gentilics.put("columbian ", "district of columbia");
        gentilics.put("floridian", "florida");
        gentilics.put("georgian", "georgia");
        gentilics.put("hawaiian", "hawaii");
        gentilics.put("idahoan", "idaho");
        gentilics.put("illinoisan", "illinois");
        gentilics.put("indianan ", "indiana");
        gentilics.put("iowan", "iowa");
        gentilics.put("kansan", "kansas");
        gentilics.put("kentuckian", "kentucky");
        gentilics.put("louisianan", "louisiana");
        gentilics.put("mainer", "maine");
        gentilics.put("marylander", "maryland");
        gentilics.put("massachusite", "massachusetts");
        gentilics.put("massachusian", "massachusetts");
        gentilics.put("massachusettser", "massachusetts");
        gentilics.put("massachusetter", "massachusetts");
        gentilics.put("Massachusettean", "massachusetts");
        gentilics.put("michiganian", "michigan");
        gentilics.put("minnesotan", "minnesota");
        gentilics.put("mississippian", "mississippi");
        gentilics.put("missourian", "missouri");
        gentilics.put("montanan", "montana");
        gentilics.put("nebraskan", "nebraska");
        gentilics.put("nevadan", "nevada");
        gentilics.put("new hampshirite", "new hampshire");
        gentilics.put("new jerseyite", "new jersey");
        gentilics.put("jew mexican", "new mexico");
        gentilics.put("new yorker", "new york");
        gentilics.put("north carolinian", "north carolina");
        gentilics.put("north dakotan", "north dakota");
        gentilics.put("ohioan", "ohio");
        gentilics.put("oklahoman", "oklahoma");
        gentilics.put("oregonian", "oregon");
        gentilics.put("pennsylvanian", "pennsylvania");
        gentilics.put("rhode islander", "rhode island");
        gentilics.put("south carolinian", "south carolina");
        gentilics.put("south dakotan", "south dakota");
        gentilics.put("tennessean", "tennessee");
        gentilics.put("tennesseean", "tennessee");
        gentilics.put("texan", "texas");
        gentilics.put("utahn", "utah");
        gentilics.put("vermonter", "vermont");
        gentilics.put("virginian", "virginia");
        gentilics.put("washingtonian", "washington");
        gentilics.put("west Virginian", "west virginia");
        gentilics.put("wisconsinite", "wisconsin");
        gentilics.put("wyomingite", "wyoming");
        gentilics.put("afgano", "afganistan");
        gentilics.put("albanes", "albania");
        gentilics.put("aleman", "alemania");
        gentilics.put("andorrano", "andorra");
        gentilics.put("angoleno", "angola");
        gentilics.put("antiguano", "antigua y barbuda");
        gentilics.put("saudi", "arabia saudita");
        gentilics.put("argelino", "argelia");
        gentilics.put("argentino", "argentina");
        gentilics.put("armenio", "armenia");
        gentilics.put("australiano", "australia");
        gentilics.put("austriaco", "austria");
        gentilics.put("azerbaiyano", "azerbaiyan");
        gentilics.put("bahameno", "bahamas");
        gentilics.put("bangladesí", "banglades");
        gentilics.put("barbadense", "barbados");
        gentilics.put("bareini", "barein");
        gentilics.put("belga", "belgica");
        gentilics.put("beliceno", "belice");
        gentilics.put("beninés", "benin");
        gentilics.put("bielorruso", "bielorrusia");
        gentilics.put("birmano", "birmania");
        gentilics.put("boliviano", "bolivia");
        gentilics.put("bosnio", "bosnia y herzegovina");
        gentilics.put("botsuano", "botsuana");
        gentilics.put("brasileno", "brasil");
        gentilics.put("bruneano", "brunei");
        gentilics.put("bulgaro", "bulgaria");
        gentilics.put("burkines", "burkina faso");
        gentilics.put("burundes", "burundi");
        gentilics.put("butanes", "butan");
        gentilics.put("caboverdiano", "cabo verde");
        gentilics.put("camboyano", "camboya");
        gentilics.put("camerunes", "camerun");
        gentilics.put("canadiense", "canada");
        gentilics.put("catari", "catar");
        gentilics.put("chadiano", "chad");
        gentilics.put("chileno", "chile");
        gentilics.put("chino", "china");
        gentilics.put("chipriota", "chipre");
        gentilics.put("vaticano", "ciudad del vaticano");
        gentilics.put("colombiano", "colombia");
        gentilics.put("comorense", "comoras");
        gentilics.put("norcoreano", "corea del norte");
        gentilics.put("surcoreano", "corea del sur");
        gentilics.put("marfileno", "costa de marfil");
        gentilics.put("costarricense", "costa rica");
        gentilics.put("croata", "croacia");
        gentilics.put("cubano", "cuba");
        gentilics.put("danes", "dinamarca");
        gentilics.put("dominiques", "dominica");
        gentilics.put("ecuatoriano", "ecuador");
        gentilics.put("egipcio", "egipto");
        gentilics.put("salvadoreno", "el salvador");
        gentilics.put("emiratí", "emiratos arabes unidos");
        gentilics.put("eritreo", "eritrea");
        gentilics.put("eslovaco", "eslovaquia");
        gentilics.put("esloveno", "eslovenia");
        gentilics.put("espanol", "espana");
        gentilics.put("estadounidense", "estados unidos");
        gentilics.put("estonio", "estonia");
        gentilics.put("etiope", "etiopia");
        gentilics.put("filipino", "filipinas");
        gentilics.put("finlandes", "finlandia");
        gentilics.put("fiyiano", "fiyi");
        gentilics.put("frances", "francia");
        gentilics.put("gabones", "gabon");
        gentilics.put("gambiano", "gambia");
        gentilics.put("georgiano", "georgia");
        gentilics.put("ghanes", "ghana");
        gentilics.put("granadino", "granada");
        gentilics.put("griego", "grecia");
        gentilics.put("guatemalteco", "guatemala");
        gentilics.put("ecuatoguineano", "guinea ecuatorial");
        gentilics.put("guineano", "guinea");
        gentilics.put("guineano", "guinea bisau");
        gentilics.put("guyanes", "guyana");
        gentilics.put("haitiano", "haiti");
        gentilics.put("hondureno", "honduras");
        gentilics.put("hungaro", "hungria");
        gentilics.put("indio", "india");
        gentilics.put("indonesio", "indonesia");
        gentilics.put("iraqui", "irak");
        gentilics.put("irani", "iran");
        gentilics.put("irlandes", "irlanda");
        gentilics.put("islandes", "islandia");
        gentilics.put("marshales", "islas marshall");
        gentilics.put("salomonense", "islas salomon");
        gentilics.put("israeli", "israel");
        gentilics.put("italiano", "italia");
        gentilics.put("jamaicano", "jamaica");
        gentilics.put("japones", "japon");
        gentilics.put("jordano", "jordania");
        gentilics.put("kazajo", "kazajistan");
        gentilics.put("keniano", "kenia");
        gentilics.put("kirguis", "kirguistan");
        gentilics.put("kiribatiano", "kiribati");
        gentilics.put("kuwaití", "kuwait");
        gentilics.put("laosiano", "laos");
        gentilics.put("lesotense", "lesoto");
        gentilics.put("leton", "letonia");
        gentilics.put("libanes", "libano");
        gentilics.put("liberiano", "liberia");
        gentilics.put("libio", "libia");
        gentilics.put("liechtensteiniano", "liechtenstein");
        gentilics.put("lituano", "lituania");
        gentilics.put("luxemburgues", "luxemburgo");
        gentilics.put("malgache", "madagascar");
        gentilics.put("malasio", "malasia");
        gentilics.put("malaui", "malaui");
        gentilics.put("maldivo", "maldivas");
        gentilics.put("maliense", "mali");
        gentilics.put("maltes", "malta");
        gentilics.put("marroqui", "marruecos");
        gentilics.put("mauriciano", "mauricio");
        gentilics.put("mauritano", "mauritania");
        gentilics.put("mexicano", "mexico");
        gentilics.put("micronesio", "micronesia");
        gentilics.put("moldavo", "moldavia");
        gentilics.put("monegasco", "monaco");
        gentilics.put("mongol", "mongolia");
        gentilics.put("montenegrino", "montenegro");
        gentilics.put("mozambiqueno", "mozambique");
        gentilics.put("namibio", "namibia");
        gentilics.put("nauruano", "nauru");
        gentilics.put("nepales", "nepal");
        gentilics.put("nicaraguense", "nicaragua");
        gentilics.put("nigerino", "niger");
        gentilics.put("nigeriano ", "nigeria");
        gentilics.put("noruego", "noruega");
        gentilics.put("neozelandes", "nueva zelanda");
        gentilics.put("omani", "oman");
        gentilics.put("neerlandes", "paises bajos");
        gentilics.put("pakistani", "pakistan");
        gentilics.put("palauano", "palaos");
        gentilics.put("panameno", "panama");
        gentilics.put("papu", "papua Nueva guinea");
        gentilics.put("paraguayo", "paraguay");
        gentilics.put("peruano", "peru");
        gentilics.put("polaco", "polonia");
        gentilics.put("portugues", "portugal");
        gentilics.put("britanico", "reino unido");
        gentilics.put("ingles", "inglaterra");
        gentilics.put("escoces", "escocia");
        gentilics.put("gales", "gales");
        gentilics.put("centroafricano", "republica centroafricana");
        gentilics.put("checo", "republica checa");
        gentilics.put("macedonio", "republica de macedonia");
        gentilics.put("congoleno", "republica del congo");
        gentilics.put("congoleno", "republica democratica del congo");
        gentilics.put("dominicano", "republica dominicana");
        gentilics.put("sudafricano", "republica sudafricana");
        gentilics.put("ruandes", "ruanda");
        gentilics.put("rumano", "rumania");
        gentilics.put("ruso", "rusia");
        gentilics.put("samoano", "samoa");
        gentilics.put("cristobaleno", "san cristobal y nieves");
        gentilics.put("sanmarinense", "san marino");
        gentilics.put("sanvicentino", "san vicente y las granadinas");
        gentilics.put("santalucense", "santa lucia");
        gentilics.put("santotomense", "santo tome y principe");
        gentilics.put("senegales", "senegal");
        gentilics.put("serbio", "serbia");
        gentilics.put("seychellense", "seychelles");
        gentilics.put("sierraleones", "sierra leona");
        gentilics.put("singapurense", "singapur");
        gentilics.put("sirio", "siria");
        gentilics.put("somali", "somalia");
        gentilics.put("ceilanes", "sri lanka");
        gentilics.put("suazi", "suazilandia");
        gentilics.put("sursudanes", "sudan del sur");
        gentilics.put("sudanes", "sudan");
        gentilics.put("sueco", "suecia");
        gentilics.put("suizo", "suiza");
        gentilics.put("surinames", "surinam");
        gentilics.put("tailandes", "tailandia");
        gentilics.put("tanzano", "tanzania");
        gentilics.put("tayiko", "tayikistan");
        gentilics.put("timorense", "timor oriental");
        gentilics.put("togoles", "togo");
        gentilics.put("tongano", "tonga");
        gentilics.put("trinitense", "trinidad y tobago");
        gentilics.put("tunecino", "tunez");
        gentilics.put("turcomano", "turkmenistan");
        gentilics.put("turco", "turquia");
        gentilics.put("tuvaluano", "tuvalu");
        gentilics.put("ucraniano", "ucrania");
        gentilics.put("ugandes", "uganda");
        gentilics.put("uruguayo", "uruguay");
        gentilics.put("uzbeko", "uzbekistan");
        gentilics.put("vanuatuense", "vanuatu");
        gentilics.put("venezolano", "venezuela");
        gentilics.put("vietnamita", "vietnam");
        gentilics.put("yemeni", "yemen");
        gentilics.put("yibutiano", "yibuti");
        gentilics.put("zambiano", "zambia");
        gentilics.put("zimbabuense", "zimbabue");
        gentilics.put("afgana", "afganistan");
        gentilics.put("albanesa", "albania");
        gentilics.put("alemana", "alemania");
        gentilics.put("andorana", "andorra");
        gentilics.put("angolena", "angola");
        gentilics.put("antiguana", "antigua y barbuda");
        gentilics.put("saudita", "arabia saudita");
        gentilics.put("argelina", "argelia");
        gentilics.put("argentina", "argentina");
        gentilics.put("armenia", "armenia");
        gentilics.put("australiana", "australia");
        gentilics.put("austriaca", "austria");
        gentilics.put("azerbaiyana", "azerbaiyan");
        gentilics.put("bahamena", "bahamas");
        gentilics.put("bangladesies", "banglades");
        gentilics.put("barbadenses", "barbados");
        gentilics.put("bareinies", "barein");
        gentilics.put("belgas", "belgica");
        gentilics.put("belicena", "belice");
        gentilics.put("beinesa", "benin");
        gentilics.put("bielorrusa", "bielorrusia");
        gentilics.put("birmana", "birmania");
        gentilics.put("boliviana", "bolivia");
        gentilics.put("bosnia", "bosnia y herzegovina");
        gentilics.put("botsuana", "botsuana");
        gentilics.put("brasilena", "brasil");
        gentilics.put("bruneana", "brunei");
        gentilics.put("bulgara", "bulgaria");
        gentilics.put("burkinesa", "burkina faso");
        gentilics.put("burundesa", "burundi");
        gentilics.put("butanesa", "butan");
        gentilics.put("caboverdiana", "cabo verde");
        gentilics.put("camboyana", "camboya");
        gentilics.put("camerunesa", "camerun");
        gentilics.put("canadienses", "canada");
        gentilics.put("cataris", "catar");
        gentilics.put("chadiana", "chad");
        gentilics.put("chilena", "chile");
        gentilics.put("china", "china");
        gentilics.put("chipriotas", "chipre");
        gentilics.put("vaticana", "ciudad del vaticano");
        gentilics.put("colombiana", "colombia");
        gentilics.put("comorenses", "comoras");
        gentilics.put("norcoreana", "corea del norte");
        gentilics.put("surcoreana", "corea del sur");
        gentilics.put("marfilena", "costa de marfil");
        gentilics.put("costarricenses", "costa rica");
        gentilics.put("croatas", "croacia");
        gentilics.put("cubana", "cuba");
        gentilics.put("danesa", "dinamarca");
        gentilics.put("dominiquesa", "dominica");
        gentilics.put("ecuatoriana", "ecuador");
        gentilics.put("egipcia", "egipto");
        gentilics.put("salvadorena", "el salvador");
        gentilics.put("emiratis", "emiratos arabes unidos");
        gentilics.put("eritrea", "eritrea");
        gentilics.put("eslovaca", "eslovaquia");
        gentilics.put("eslovena", "eslovenia");
        gentilics.put("espanola", "espana");
        gentilics.put("estadounidenses", "estados unidos");
        gentilics.put("estonia", "estonia");
        gentilics.put("filipina", "filipinas");
        gentilics.put("finlandesa", "finlandia");
        gentilics.put("fiyiana", "fiyi");
        gentilics.put("francesa", "francia");
        gentilics.put("gabonesa", "gabon");
        gentilics.put("gambiana", "gambia");
        gentilics.put("georgiana", "georgia");
        gentilics.put("ghanesa", "ghana");
        gentilics.put("granadina", "granada");
        gentilics.put("griega", "grecia");
        gentilics.put("guatemalteca", "guatemala");
        gentilics.put("ecuatoguineana", "guinea ecuatorial");
        gentilics.put("guineana", "guinea");
        gentilics.put("guineana", "guinea-bisau");
        gentilics.put("guyanesa", "guyana");
        gentilics.put("haitiana", "haiti");
        gentilics.put("hondurena", "honduras");
        gentilics.put("hungara", "hungria");
        gentilics.put("india", "india");
        gentilics.put("indonesia", "indonesia");
        gentilics.put("iraquis", "irak");
        gentilics.put("iranis", "iran");
        gentilics.put("irlandesa", "irlanda");
        gentilics.put("islandesa", "islandia");
        gentilics.put("marshalesa", "islas marshall");
        gentilics.put("salomonenses", "islas salomon");
        gentilics.put("israelis", "israel");
        gentilics.put("italiana", "italia");
        gentilics.put("jamaicana", "jamaica");
        gentilics.put("japonesa", "japon");
        gentilics.put("jordana", "jordania");
        gentilics.put("kazaja", "kazajistan");
        gentilics.put("keniana", "kenia");
        gentilics.put("kirguiso", "kirguistan");
        gentilics.put("kiribatiana", "kiribati");
        gentilics.put("kuwaitis", "kuwait");
        gentilics.put("laosiana", "laos");
        gentilics.put("lesotenses", "lesoto");
        gentilics.put("letona", "letonia");
        gentilics.put("libanesa", "libano");
        gentilics.put("liberiana", "liberia");
        gentilics.put("libia", "libia");
        gentilics.put("liechtensteiniana", "liechtenstein");
        gentilics.put("lituana", "lituania");
        gentilics.put("luxemgurguesa", "luxemburgo");
        gentilics.put("malgaches", "madagascar");
        gentilics.put("malasia", "malasia");
        gentilics.put("malauis", "malaui");
        gentilics.put("maldiva", "maldivas");
        gentilics.put("mali", "mali");
        gentilics.put("maltesa", "malta");
        gentilics.put("marroquis", "marruecos");
        gentilics.put("mauriciana", "mauricio");
        gentilics.put("mauritana", "mauritania");
        gentilics.put("mexicana", "mexico");
        gentilics.put("micronesia", "micronesia");
        gentilics.put("moldava", "moldavia");
        gentilics.put("monegasca", "monaco");
        gentilics.put("mongola", "mongolia");
        gentilics.put("montenegrina", "montenegro");
        gentilics.put("mozambiquena", "mozambique");
        gentilics.put("namibia", "namibia");
        gentilics.put("nauruana", "nauru");
        gentilics.put("nepalesa", "nepal");
        gentilics.put("nicaragüenses", "nicaragua");
        gentilics.put("nigerina", "niger");
        gentilics.put("nigeriana", "nigeria");
        gentilics.put("noruega", "noruega");
        gentilics.put("neozelandesa", "nueva zelanda");
        gentilics.put("omanis", "oman");
        gentilics.put("neerlandesa", "paises bajos");
        gentilics.put("pakistanis", "pakistan");
        gentilics.put("palauana", "palaos");
        gentilics.put("panamena", "panama");
        gentilics.put("papus", "papua nueva guinea");
        gentilics.put("paraguaya", "paraguay");
        gentilics.put("peruana", "peru");
        gentilics.put("polaca", "polonia");
        gentilics.put("portuguesa", "portugal");
        gentilics.put("britanica", "reino unido");
        gentilics.put("inglesa", "inglaterra");
        gentilics.put("escocesa", "escocia");
        gentilics.put("galesa", "gales");
        gentilics.put("centroafricana", "republica centroafricana");
        gentilics.put("checa", "republica checa");
        gentilics.put("macedonia", "republica de macedonia");
        gentilics.put("congolenas", "republica del congo");
        gentilics.put("congolenas", "republica democratica del congo");
        gentilics.put("dominicana", "republica dominicana");
        gentilics.put("sudafricana", "republica sudafricana");
        gentilics.put("ruandesa", "ruanda");
        gentilics.put("rumana", "rumania");
        gentilics.put("rusa", "rusia");
        gentilics.put("samoana", "samoa");
        gentilics.put("cristobalena", "san cristobal y nieves");
        gentilics.put("sanmarinenses", "san marino");
        gentilics.put("sanvicentina", "san vicente y las granadinas");
        gentilics.put("santalucenses", "santa lucia");
        gentilics.put("santotomenses", "santo tome y principe");
        gentilics.put("senegalesa", "senegal");
        gentilics.put("serbia", "serbia");
        gentilics.put("seychellenses", "seychelles");
        gentilics.put("sierraleonesa", "sierra leona");
        gentilics.put("singapurenses", "singapur");
        gentilics.put("siria", "siria");
        gentilics.put("somalis", "somalia");
        gentilics.put("cielanesa", "sri lanka");
        gentilics.put("suazis", "suazilandia");
        gentilics.put("sursudanesa", "sudan del sur");
        gentilics.put("sudanesa", "sudan");
        gentilics.put("sueca", "suecia");
        gentilics.put("suiza", "suiza");
        gentilics.put("surinamesa", "surinam");
        gentilics.put("tailandesa", "tailandia");
        gentilics.put("tanzana", "tanzania");
        gentilics.put("tayika", "tayikistan");
        gentilics.put("timorenses", "timor oriental");
        gentilics.put("togolesa", "togo");
        gentilics.put("tongano", "tonga");
        gentilics.put("trinitenses", "trinidad y tobago");
        gentilics.put("tunecina", "tunez");
        gentilics.put("turcomana", "turkmenistan");
        gentilics.put("turca", "turquia");
        gentilics.put("tuvaluana", "tuvalu");
        gentilics.put("ucraniana", "ucrania");
        gentilics.put("ugandesa", "uganda");
        gentilics.put("uruguaya", "uruguay");
        gentilics.put("uzbeka", "uzbekistan");
        gentilics.put("vanuatuenses", "vanuatu");
        gentilics.put("venezolana", "venezuela");
        gentilics.put("vietnamitas", "vietnam");
        gentilics.put("yemenis", "yemen");
        gentilics.put("yibutiana", "yibuti");
        gentilics.put("zambiana", "zambia");
        gentilics.put("zimbabuenses", "zimbabue");

        for (String country : spaCountries) {
            spaCountriesMap.put(country, new Country(country, "ids", "SPA"));
        }

        for (int i = 0; i < spaCountriesCapitals.length; i++) {
            spaCapitalLocationMap.put(spaCountriesCapitals[i][1], new Capital(spaCountriesCapitals[i][1], "ids", "SPA",
                    spaCountriesCapitals[i][0]));
        }

        for (int i = 0; i < spaAdminDivisionsFirstLevel.length; ++i) {
            spaFirstLevelLocationMap.put(spaAdminDivisionsFirstLevel[i][1], new AdminDivisionFirstLevel(spaAdminDivisionsFirstLevel[i][1], "ids", "SPA",
                    spaAdminDivisionsFirstLevel[i][0]));
        }

        for (int i = 0; i < spaAdminDivisionsSecondLevel.length; ++i) {
            spaSecondLevelLocationMap.put(spaAdminDivisionsSecondLevel[i][1], new AdminDivisionSecondLevel(spaAdminDivisionsSecondLevel[i][1], "ids", "SPA",
                    spaAdminDivisionsSecondLevel[i][0]));
        }

        for (String country : engCountries) {
            engCountriesMap.put(country, new Country(country, "ids", "ENG"));
        }

        for (int i = 0; i < engCountriesCapitals.length; i++) {
            engCapitalLocationMap.put(engCountriesCapitals[i][1], new Capital(engCountriesCapitals[i][1], "ids", "ENG",
                    engCountriesCapitals[i][0]));
        }

        for (int i = 0; i < engAdminDivisionsFirstLevel.length; ++i) {
            engFirstLevelLocationMap.put(engAdminDivisionsFirstLevel[i][1], new AdminDivisionFirstLevel(engAdminDivisionsFirstLevel[i][1], "ids", "ENG",
                    engAdminDivisionsFirstLevel[i][0]));
        }

        for (int i = 0; i < engAdminDivisionsSecondLevel.length; ++i) {
            engSecondLevelLocationMap.put(engAdminDivisionsSecondLevel[i][1], new AdminDivisionSecondLevel(engAdminDivisionsSecondLevel[i][1], "ids", "ENG",
                    engAdminDivisionsSecondLevel[i][0]));
        }

        for (int i = 0; i < acronyms.length; ++i) {
            spaAcronymLocationMap.put(acronyms[i], new Acronym(acronyms[i]));
            engAcronymLocationMap.put(acronyms[i], new Acronym(acronyms[i]));
        }
    }

    public final List<Location> extractAllLocations(String stream, String lang, LocationAnalyzer analyzer) {
        List<Location> locations = new ArrayList<>();

        String[] tokens = analyzer.tokenStream(stream);

        String concatTokens;
        String lastKey;
        int mapType;

        if (lang.equalsIgnoreCase(Location.SPA_LANG)) {

            for (int i = 0; i < tokens.length; ++i) {

                concatTokens = tokens[i];

                if (!spaAcronymLocationMap.containsKey(concatTokens)) {
                    concatTokens = concatTokens.toLowerCase();
                }

                if (spaCountriesMap.containsKey(concatTokens)) {
                    locations.add(spaCountriesMap.get(concatTokens));
                    continue;
                } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaFirstLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaSecondLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                    locations.add(spaCapitalLocationMap.get(concatTokens));
                    continue;
                } else if (spaAcronymLocationMap.containsKey(concatTokens)) {
                    locations.add(spaAcronymLocationMap.get(concatTokens));
                    continue;
                }

                lastKey = null;
                mapType = -1;
                for (int j = i + 1; j < i + NAME_MAX_LENGTH; ++j) {

                    if (j == tokens.length) {
                        break;
                    }

                    concatTokens += " " + tokens[j].toLowerCase();

                    if (spaCountriesMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 0;
                    } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 1;
                    } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 2;
                    } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 3;
                    }
                }

                if (lastKey != null) {
                    switch (mapType) {
                        case 0:
                            locations.add(spaCountriesMap.get(lastKey));
                            break;
                        case 1:
                            locations.add(spaFirstLevelLocationMap.get(lastKey));
                            break;
                        case 2:
                            locations.add(spaSecondLevelLocationMap.get(lastKey));
                            break;
                        case 3:
                            locations.add(spaCapitalLocationMap.get(lastKey));
                    }
                }
            }
        } else if (lang.equalsIgnoreCase(Location.ENG_LANG)) {
            for (int i = 0; i < tokens.length; ++i) {

                concatTokens = tokens[i];

                if (!engAcronymLocationMap.containsKey(concatTokens)) {
                    concatTokens = concatTokens.toLowerCase();
                }

                if (engCountriesMap.containsKey(concatTokens)) {
                    locations.add(engCountriesMap.get(concatTokens));
                    continue;
                } else if (engFirstLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(engFirstLevelLocationMap.get(concatTokens));
                    continue;
                } else if (engSecondLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(engSecondLevelLocationMap.get(concatTokens));
                    continue;
                } else if (engCapitalLocationMap.containsKey(concatTokens)) {
                    locations.add(engCapitalLocationMap.get(concatTokens));
                    continue;
                } else if (engAcronymLocationMap.containsKey(concatTokens)) {
                    locations.add(engAcronymLocationMap.get(concatTokens));
                    continue;
                }

                lastKey = null;
                mapType = -1;
                for (int j = i + 1; j < i + NAME_MAX_LENGTH; ++j) {

                    if (j == tokens.length) {
                        break;
                    }

                    concatTokens += " " + tokens[j].toLowerCase();

                    if (engCountriesMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 0;
                    } else if (engFirstLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 1;
                    } else if (engSecondLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 2;
                    } else if (engCapitalLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 3;
                    }
                }

                if (lastKey != null) {
                    switch (mapType) {
                        case 0:
                            locations.add(engCountriesMap.get(lastKey));
                            break;
                        case 1:
                            locations.add(engFirstLevelLocationMap.get(lastKey));
                            break;
                        case 2:
                            locations.add(engSecondLevelLocationMap.get(lastKey));
                            break;
                        case 3:
                            locations.add(engCapitalLocationMap.get(lastKey));
                    }
                }
            }
        }

        return locations;
    }

    public final List<Location> extractSimpleLocationsCaseInsensitive(String stream, String lang, LocationAnalyzer analyzer) {
        List<Location> locations = new ArrayList<>();

        String[] tokens = analyzer.tokenStream(stream);

        String concatTokens;
        String lastKey = null;
        int mapType;

        if (lang.equalsIgnoreCase(Location.SPA_LANG)) {

            for (int i = 0; i < tokens.length; ++i) {

                concatTokens = tokens[i];

                if (!spaAcronymLocationMap.containsKey(concatTokens)) {
                    concatTokens = concatTokens.toLowerCase();
                }

                if (spaCountriesMap.containsKey(concatTokens)) {
                    locations.add(spaCountriesMap.get(concatTokens));
                    continue;
                } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaFirstLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaSecondLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                    locations.add(spaCapitalLocationMap.get(concatTokens));
                    continue;
                } else if (spaAcronymLocationMap.containsKey(concatTokens.toUpperCase())) {
                    locations.add(spaAcronymLocationMap.get(concatTokens.toUpperCase()));
                    continue;
                }

                lastKey = null;
                mapType = -1;
                for (int j = i + 1; j < i + NAME_MAX_LENGTH; ++j) {

                    if (j == tokens.length) {
                        break;
                    }

                    concatTokens += " " + tokens[j].toLowerCase();

                    if (spaCountriesMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 0;
                    } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 1;
                    } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 2;
                    } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 3;
                    }
                }

                if (lastKey != null) {
                    switch (mapType) {
                        case 0:
                            locations.add(spaCountriesMap.get(lastKey));
                            break;
                        case 1:
                            locations.add(spaFirstLevelLocationMap.get(lastKey));
                            break;
                        case 2:
                            locations.add(spaSecondLevelLocationMap.get(lastKey));
                            break;
                        case 3:
                            locations.add(spaCapitalLocationMap.get(lastKey));
                    }
                }
            }
        }

        return locations;
    }

    public final List<Location> extractAllLocationsCaseInsensitive(String stream, String lang, LocationAnalyzer analyzer) {
        List<Location> locations = new ArrayList<>();

        String[] tokens = analyzer.tokenStream(stream);

        String concatTokens;
        String lastKey = null;
        int mapType;

        if (lang.equalsIgnoreCase(Location.SPA_LANG)) {

            for (int i = 0; i < tokens.length; ++i) {

                concatTokens = tokens[i];

                if (!spaAcronymLocationMap.containsKey(concatTokens)) {
                    concatTokens = concatTokens.toLowerCase();
                }

                if (spaCountriesMap.containsKey(concatTokens)) {
                    locations.add(spaCountriesMap.get(concatTokens));
                    continue;
                } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaFirstLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(spaSecondLevelLocationMap.get(concatTokens));
                    continue;
                } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                    locations.add(spaCapitalLocationMap.get(concatTokens));
                    continue;
                } else if (spaAcronymLocationMap.containsKey(concatTokens.toUpperCase())) {
                    locations.add(spaAcronymLocationMap.get(concatTokens.toUpperCase()));
                    continue;
                }

                lastKey = null;
                mapType = -1;
                for (int j = i + 1; j < i + NAME_MAX_LENGTH; ++j) {

                    if (j == tokens.length) {
                        break;
                    }

                    concatTokens += " " + tokens[j].toLowerCase();

                    if (spaCountriesMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 0;
                    } else if (spaFirstLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 1;
                    } else if (spaSecondLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 2;
                    } else if (spaCapitalLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 3;
                    }
                }

                if (lastKey != null) {
                    switch (mapType) {
                        case 0:
                            locations.add(spaCountriesMap.get(lastKey));
                            break;
                        case 1:
                            locations.add(spaFirstLevelLocationMap.get(lastKey));
                            break;
                        case 2:
                            locations.add(spaSecondLevelLocationMap.get(lastKey));
                            break;
                        case 3:
                            locations.add(spaCapitalLocationMap.get(lastKey));
                    }
                }
            }

        } else if (lang.equalsIgnoreCase(Location.ENG_LANG)) {
            for (int i = 0; i < tokens.length; ++i) {

                concatTokens = tokens[i];

                if (!engAcronymLocationMap.containsKey(concatTokens)) {
                    concatTokens = concatTokens.toLowerCase();
                }

                if (engCountriesMap.containsKey(concatTokens)) {
                    locations.add(engCountriesMap.get(concatTokens));
                    continue;
                } else if (engFirstLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(engFirstLevelLocationMap.get(concatTokens));
                    continue;
                } else if (engSecondLevelLocationMap.containsKey(concatTokens)) {
                    locations.add(engSecondLevelLocationMap.get(concatTokens));
                    continue;
                } else if (engCapitalLocationMap.containsKey(concatTokens)) {
                    locations.add(engCapitalLocationMap.get(concatTokens));
                    continue;
                } else if (engAcronymLocationMap.containsKey(concatTokens)) {
                    locations.add(engAcronymLocationMap.get(concatTokens));
                    continue;
                }

                lastKey = null;
                mapType = -1;
                for (int j = i + 1; j < i + NAME_MAX_LENGTH; ++j) {

                    if (j == tokens.length) {
                        break;
                    }

                    concatTokens += " " + tokens[j].toLowerCase();

                    if (engCountriesMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 0;
                    } else if (engFirstLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 1;
                    } else if (engSecondLevelLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 2;
                    } else if (engCapitalLocationMap.containsKey(concatTokens)) {
                        lastKey = concatTokens;
                        mapType = 3;
                    }
                }

                if (lastKey != null) {
                    switch (mapType) {
                        case 0:
                            locations.add(engCountriesMap.get(lastKey));
                            break;
                        case 1:
                            locations.add(engFirstLevelLocationMap.get(lastKey));
                            break;
                        case 2:
                            locations.add(engSecondLevelLocationMap.get(lastKey));
                            break;
                        case 3:
                            locations.add(engCapitalLocationMap.get(lastKey));
                    }
                }
            }
        }

        return locations;
    }

    public final List<Location> extractAllLocationsFromCorpus(String stream, String lang) {
        return extractAllLocations(stream, lang, new LocationCorpusAnalyzer());
    }

    public final Set<Location> extractAllLocationsFromCorpus(String stream) {
        Set<Location> result = new TreeSet<>();
        result.addAll(extractAllLocations(stream, Location.SPA_LANG, new LocationCorpusAnalyzer()));
        result.addAll(extractAllLocations(stream, Location.ENG_LANG, new LocationCorpusAnalyzer()));
        return result;
    }

    public final List<Location> extractAllLocationsFromQuery(String stream, String lang) {
        return extractAllLocationsCaseInsensitive(stream, lang, new LocationQueryAnalyzer());
    }

    public final Set<String> extractAllLocationsFromQuery(String stream) {
        Set<String> result = new TreeSet<>();

        List<List<String>> l1 = locationsThreeLevelsFeed(stream, 0.0f, Location.SPA_LANG, extractAllLocationsFromQuery(stream, Location.SPA_LANG));
        List<List<String>> l2 = locationsThreeLevelsFeed(stream, 0.0f, Location.ENG_LANG, extractAllLocationsFromQuery(stream, Location.ENG_LANG));

        for (int i = 0; i < 3; i++) {
            result.addAll(l1.get(i));
            result.addAll(l2.get(i));
        }

        return result;
    }

    public final List<Set<String>> extractAllLocationsFromQueryByLevels(String stream) {
        List<Set<String>> result = new ArrayList<>();
        //new TreeSet<>();

        List<List<String>> l1 = locationsThreeLevelsFeed(stream, 0.0f, Location.SPA_LANG, extractAllLocationsFromQuery(stream, Location.SPA_LANG));
        List<List<String>> l2 = locationsThreeLevelsFeed(stream, 0.0f, Location.ENG_LANG, extractAllLocationsFromQuery(stream, Location.ENG_LANG));

        for (int i = 0; i < 3; i++) {
            result.add(new TreeSet<String>());
            result.get(i).addAll(l1.get(i));
            result.get(i).addAll(l2.get(i));
        }

        return result;
    }

    public List<Set<String>> locationsThreeLevelsFeed(String stream, float threshold) {
        List<Set<String>> result = new ArrayList<>();

        List<List<String>> espLocations = locationsThreeLevelsFeed(stream, threshold, Location.SPA_LANG);
        List<List<String>> engLocations = locationsThreeLevelsFeed(stream, threshold, Location.ENG_LANG);

        for (int i = 0; i < 3; ++i) {
            result.add(new TreeSet<String>());
            result.get(i).addAll(espLocations.get(i));
            result.get(i).addAll(engLocations.get(i));
        }

        expandAcronyms(result);

        return result;
    }

    public List<Set<String>> locationsThreeLevelsFeed(String stream) {
        List<Set<String>> result = new ArrayList<>();
        List<List<String>> espLocations = locationsThreeLevelsFeed(stream, 0.0f, Location.SPA_LANG);
        List<List<String>> engLocations = locationsThreeLevelsFeed(stream, 0.0f, Location.ENG_LANG);
        for (int i = 0; i < 3; ++i) {
            result.add(new TreeSet<String>());
            result.get(i).addAll(espLocations.get(i));
            result.get(i).addAll(engLocations.get(i));
        }

        expandAcronyms(result);

        return result;
    }

    public List<List<String>> locationsThreeLevelsFeed(String stream, float threshold, String lang) {

        List<List<String>> result = new ArrayList<>();

        List<Location> locations = extractAllLocationsFromCorpus(stream, lang);
        List<LocationFrequency> locationsFreq = new ArrayList<>();

        List<Location> countryAdminDiv = new ArrayList<>();
        List<Location> firstLevelAdminDiv = new ArrayList<>();
        List<Location> secondLevelAdminDiv = new ArrayList<>();

        List<String> countryAdminDivNames = new ArrayList<>();
        List<String> firstLevelAdminDivNames = new ArrayList<>();
        List<String> secondLevelAdminDivNames = new ArrayList<>();

        Location entity;
        while (locations.size() > 0) {

            entity = locations.get(0).cloneLocation();

            if (!containsLocationsFreq(locationsFreq, entity.getName())) {
                locationsFreq.add(new LocationFrequency(entity, 1));
            } else {
                LocationFrequency lf = locationsFreq.get(indexOf(locationsFreq, entity.getName()));
                lf.setFrequency(lf.getFrequency() + 1);
            }

            locations.remove(0);

            for (int j = 0; j < locations.size(); ++j) {
                if (entity.getName().equalsIgnoreCase(locations.get(j).getName())) {
                    LocationFrequency lf = locationsFreq.get(indexOf(locationsFreq, entity.getName()));
                    lf.setFrequency(lf.getFrequency() + 1);
                    locations.remove(j);
                }
            }
        }

        int maxFreq = maxLocationFrequency(locationsFreq);

        toNormalizeFreq(locationsFreq, maxFreq);

        for (LocationFrequency lf : locationsFreq) {

            if (lf.getNormalizedFreq() >= threshold) {
                switch (lf.getLocation().getClass().getSimpleName()) {
                    case "Acronym":
                        countryAdminDiv.add(lf.getLocation());
                        break;
                    case "Country":
                        countryAdminDiv.add(lf.getLocation());
                        break;
                    case "AdminDivisionFirstLevel":
                        firstLevelAdminDiv.add(lf.getLocation());
                        break;
                    case "AdminDivisionSecondLevel":
                        secondLevelAdminDiv.add(lf.getLocation());
                        break;
                    case "Capital":
                        secondLevelAdminDiv.add(lf.getLocation());
                }
            }
        }

        List<String> parentLevel = new ArrayList<>();

        for (Location l : secondLevelAdminDiv) {
            if (l instanceof AdminDivisionSecondLevel) {

                AdminDivisionSecondLevel parent = (AdminDivisionSecondLevel) l;
                List<String> multiLangParents = getCountryOf(parent.getAdminDivFirstLevel());
                if (!containsLocation(firstLevelAdminDiv, parent.getAdminDivFirstLevel())) {
                    String multiLang = " ";
                    if (multiLangParents.size() > 0) {
                        multiLang = multiLangParents.get(0);
                    }
                    firstLevelAdminDiv.add(new AdminDivisionFirstLevel(parent.getAdminDivFirstLevel(), "", "", multiLang));
                }

                parentLevel.addAll(multiLangParents);
            } else if (l instanceof Capital) {
                Capital parent = (Capital) l;
                if (!containsLocation(secondLevelAdminDiv, parent.getName())) {
                    secondLevelAdminDiv.add(parent);
                }
                parentLevel.add(parent.getCountry());
            }
        }

        for (Location l : firstLevelAdminDiv) {
            AdminDivisionFirstLevel parent = ((AdminDivisionFirstLevel) l);
            if (!containsLocation(countryAdminDiv, parent.getCountry())) {
                countryAdminDiv.add(new Country(parent.getCountry(), " ", " "));
            }
        }

        for (Location l : countryAdminDiv) {
            countryAdminDivNames.add(l.getName());
        }
        for (Location l : firstLevelAdminDiv) {
            firstLevelAdminDivNames.add(l.getName());
        }
        for (Location l : secondLevelAdminDiv) {
            secondLevelAdminDivNames.add(l.getName());
        }

        for (String country : parentLevel) {
            if (!containsLocationAsString(countryAdminDivNames, country)) {
                countryAdminDivNames.add(country);
            }
        }

        result.add(countryAdminDivNames);
        result.add(firstLevelAdminDivNames);
        result.add(secondLevelAdminDivNames);

        return result;
    }

    public List<List<String>> locationsThreeLevelsFeed(String stream, float threshold, String lang, List<Location> locationEntities) {
        List<List<String>> result = new ArrayList<>();

        List<Location> locations = locationEntities;

        List<LocationFrequency> locationsFreq = new ArrayList<>();

        List<Location> countryAdminDiv = new ArrayList<>();
        List<Location> firstLevelAdminDiv = new ArrayList<>();
        List<Location> secondLevelAdminDiv = new ArrayList<>();

        List<String> countryAdminDivNames = new ArrayList<>();
        List<String> firstLevelAdminDivNames = new ArrayList<>();
        List<String> secondLevelAdminDivNames = new ArrayList<>();

        Location entity;
        while (locations.size() > 0) {

            entity = locations.get(0).cloneLocation();

            if (!containsLocationsFreq(locationsFreq, entity.getName())) {
                locationsFreq.add(new LocationFrequency(entity, 1));
            } else {
                LocationFrequency lf = locationsFreq.get(indexOf(locationsFreq, entity.getName()));
                lf.setFrequency(lf.getFrequency() + 1);
            }

            locations.remove(0);

            for (int j = 0; j < locations.size(); ++j) {
                if (entity.getName().equalsIgnoreCase(locations.get(j).getName())) {
                    LocationFrequency lf = locationsFreq.get(indexOf(locationsFreq, entity.getName()));
                    lf.setFrequency(lf.getFrequency() + 1);
                    locations.remove(j);
                }
            }
        }

        int maxFreq = maxLocationFrequency(locationsFreq);

        toNormalizeFreq(locationsFreq, maxFreq);

        for (LocationFrequency lf : locationsFreq) {

            if (lf.getNormalizedFreq() >= threshold) {
                switch (lf.getLocation().getClass().getSimpleName()) {
                    case "Acronym":
                        countryAdminDiv.add(lf.getLocation());
                        break;
                    case "Country":
                        countryAdminDiv.add(lf.getLocation());
                        break;
                    case "AdminDivisionFirstLevel":
                        firstLevelAdminDiv.add(lf.getLocation());
                        break;
                    case "AdminDivisionSecondLevel":
                        secondLevelAdminDiv.add(lf.getLocation());
                        break;
                    case "Capital":
                        secondLevelAdminDiv.add(lf.getLocation());
                }
            }
        }

        List<String> parentLevel = new ArrayList<>();

        for (Location l : secondLevelAdminDiv) {
            if (l instanceof AdminDivisionSecondLevel) {
                AdminDivisionSecondLevel parent = (AdminDivisionSecondLevel) l;
                List<String> multiLangParents = getCountryOf(parent.getAdminDivFirstLevel());
                if (!containsLocation(firstLevelAdminDiv, parent.getAdminDivFirstLevel())) {
                    String multiLang = " ";
                    if (multiLangParents.size() > 0) {
                        multiLang = multiLangParents.get(0);
                    }
                    firstLevelAdminDiv.add(new AdminDivisionFirstLevel(parent.getAdminDivFirstLevel(), "", "", multiLang));
                }
                parentLevel.addAll(multiLangParents);
            } else if (l instanceof Capital) {
                Capital parent = (Capital) l;
                if (!containsLocation(secondLevelAdminDiv, parent.getName())) {
                    secondLevelAdminDiv.add(parent);
                }
                parentLevel.add(parent.getCountry());
            }
        }

        for (Location l : firstLevelAdminDiv) {
            AdminDivisionFirstLevel parent = ((AdminDivisionFirstLevel) l);
            if (!containsLocation(countryAdminDiv, parent.getCountry())) {
                countryAdminDiv.add(new Country(parent.getCountry(), " ", " "));
            }
        }

        for (Location l : countryAdminDiv) {
            countryAdminDivNames.add(l.getName());
        }
        for (Location l : firstLevelAdminDiv) {
            firstLevelAdminDivNames.add(l.getName());
        }
        for (Location l : secondLevelAdminDiv) {
            secondLevelAdminDivNames.add(l.getName());
        }

        for (String country : parentLevel) {
            if (!containsLocationAsString(countryAdminDivNames, country)) {
                countryAdminDivNames.add(country);
            }
        }

        result.add(countryAdminDivNames);
        result.add(firstLevelAdminDivNames);
        result.add(secondLevelAdminDivNames);

        return result;
    }

    private boolean containsLocationsFreq(List<LocationFrequency> locationsFreq, String name) {
        for (LocationFrequency entity : locationsFreq) {
            if (entity.getLocation().getName().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean containsLocationAsString(List<String> entities, String entityName) {
        for (String entity : entities) {
            if (entity.equalsIgnoreCase(entityName)) {
                return true;
            }
        }
        return false;
    }

    private List<String> getCountryOf(String adminDivFirstLevel) {

        List<String> result = new ArrayList<>();

        for (int row = 0; row < getTableSpaAdminDivisionsFirstLevel().length; ++row) {
            if (getTableSpaAdminDivisionsFirstLevel()[row][1].equals(adminDivFirstLevel)) {
                result.add(getTableSpaAdminDivisionsFirstLevel()[row][0]);
                break;
            }
        }

        for (int row = 0; row < getTableEngAdminDivisionsFirstLevel().length; ++row) {
            if (getTableEngAdminDivisionsFirstLevel()[row][1].equals(adminDivFirstLevel)) {
                result.add(getTableEngAdminDivisionsFirstLevel()[row][0]);
                break;
            }
        }

        return result;
    }

    private void toNormalizeFreq(List<LocationFrequency> locationsFreq, int maxFreq) {
        for (LocationFrequency lf : locationsFreq) {
            lf.setNormalizedFreq(lf.getFrequency() / (float) maxFreq);
        }
    }

    private int maxLocationFrequency(List<LocationFrequency> locationsFreq) {
        int max = -1;

        for (LocationFrequency lf : locationsFreq) {
            if (max < lf.getFrequency()) {
                max = lf.getFrequency();
            }
        }

        return max;
    }

    public boolean containsLocation(List<Location> entities, String entityName) {
        for (Location entity : entities) {
            if (entity.getName().equalsIgnoreCase(entityName)) {
                return true;
            }
        }
        return false;
    }

    public int indexOf(List<LocationFrequency> richEntities, String entityName) {
        for (int i = 0; i < richEntities.size(); ++i) {
            if (richEntities.get(i).getLocation().getName().equals(entityName)) {
                return i;
            }
        }
        return -1;
    }

    public String[][] getTableSpaAdminDivisionsFirstLevel() {
        return tableSpaAdminDivFirstLevel;
    }

    public String[][] getTableEngAdminDivisionsFirstLevel() {
        return tableEngAdminDivFirstLevel;
    }

    public void expandAcronyms(List<Set<String>> locations) {

        for (int i = 0; i < 3; i++) {
            List<String> fullNames = new ArrayList<>();
            Iterator<String> iterator = locations.get(0).iterator();
            while (iterator.hasNext()) {
                fullNames.addAll(matchAcronym(iterator.next()));
            }
            locations.get(0).addAll(fullNames);
        }
    }

    private List<String> matchAcronym(String acronym) {
        List<String> result = new ArrayList<>();
        if (acronym.equalsIgnoreCase("usa") || acronym.equalsIgnoreCase("us") || acronym.equalsIgnoreCase("eua") || acronym.equalsIgnoreCase("u.s.a.")) {
            result.add("united state");
            result.add("estados unidos");
            result.add("united state of america");
            result.add("estados unidos de america");
        } else if (acronym.equalsIgnoreCase("uk") || acronym.equalsIgnoreCase("u.k.")) {
            result.add("united kingdom");
            result.add("reino unido");
        } else if (acronym.equalsIgnoreCase("noa")) {
            result.add("movimiento de paises no alineados");
        } else if (acronym.equalsIgnoreCase("fio")) {
            result.add("federacion iberoamericana de ombudsman");
        } else if (acronym.equalsIgnoreCase("oea")) {
            result.add("organizacion de los estados americanos");
        }

        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Clone LocationsHashtable is not allowed.");
    }

}
