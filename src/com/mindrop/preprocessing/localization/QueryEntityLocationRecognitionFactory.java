package com.mindrop.preprocessing.localization;

public class QueryEntityLocationRecognitionFactory {
    private int strategy = -1;

    public static final int SMART_LOCATIONS_RECOGNITION = 10;

    public QueryEntityLocationRecognitionFactory(int strategy) {
        this.strategy = strategy;
    }

    public QueryEntityLocationRecognitionModel createQueryEntityLocationRecognitionModel() {
        QueryEntityLocationRecognitionModel model = null;

        switch (this.strategy) {
            case SMART_LOCATIONS_RECOGNITION:
                model = new SmartQueryEntityLocationRecognitionModel();
        }

        return model;
    }
}
