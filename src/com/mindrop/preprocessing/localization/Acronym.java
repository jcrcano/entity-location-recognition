package com.mindrop.preprocessing.localization;

public class Acronym implements Location, Comparable {

    String upperCaseAcronym;

    public Acronym(String upperCaseAcronym) {
        this.upperCaseAcronym = upperCaseAcronym;
    }

    public String getUpperCaseAcronym() {
        return upperCaseAcronym;
    }

    public void setUpperCaseAcronym(String upperCaseAcronym) {
        this.upperCaseAcronym = upperCaseAcronym;
    }

    @Override
    public String getName() {
        return upperCaseAcronym;
    }

    @Override
    public Location cloneLocation() {
        return new Acronym(this.getName());
    }

    @Override
    public String toString() {
        return "Acronym{" +
                "upperCaseAcronym='" + upperCaseAcronym + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (!getName().equals(location.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Location l = (Location)o;
        return this.getName().compareTo(l.getName());
    }
}
