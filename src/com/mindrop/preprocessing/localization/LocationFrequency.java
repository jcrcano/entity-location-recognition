package com.mindrop.preprocessing.localization;

public class LocationFrequency {
    private Location location;
    private int frequency;
    private float normalizedFreq;

    public LocationFrequency(Location location, int frequency) {
        this.location = location;
        this.frequency = frequency;
        this.normalizedFreq = 0.f;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public float getNormalizedFreq() {
        return normalizedFreq;
    }

    public void setNormalizedFreq(float normalizedFreq) {
        this.normalizedFreq = normalizedFreq;
    }

    @Override
    public String toString() {
        return "LocationFrequency{" +
                "location=" + location +
                ", frequency=" + frequency +
                ", normalizedFreq=" + normalizedFreq +
                '}';
    }
}
