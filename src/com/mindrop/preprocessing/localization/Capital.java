package com.mindrop.preprocessing.localization;

import java.util.Arrays;
import java.util.List;

public class Capital implements Location, Comparable {
    private List<String> fullName;
    private String isoCode;
    private String languaje;
    private String country;

    public Capital(String name, String isoCode, String languaje, String country) {
        fullName = Arrays.asList(name.split(" "));
        this.isoCode = isoCode;
        this.languaje = languaje;
        this.country = country;
    }

    @Override
    public String getName() {
        String name = "";

        for (String n : fullName) {
            name += n + " ";
        }

        return name.trim();
    }

    @Override
    public Location cloneLocation() {
        return new Capital(this.getName(), this.getIsoCode(), this.getLanguaje(), this.getCountry());
    }

    @Override
    public String toString() {
        return "Capital{" +
                "fullName=" + fullName +
                ", isoCode='" + isoCode + '\'' +
                ", languaje='" + languaje + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public String getIsoCode() {
        return isoCode;
    }

    public String getLanguaje() {
        return languaje;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (!getName().equals(location.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Location l = (Location)o;
        return this.getName().compareTo(l.getName());
    }
}
