package com.mindrop.preprocessing.localization;

import java.util.Arrays;
import java.util.List;

public class Country implements Location, Comparable {

    private List<String> fullName;
    private String isoCode;
    private String languaje;

    public Country(String name, String isoCode, String languaje) {
        fullName = Arrays.asList(name.split(" "));
        this.isoCode = isoCode;
        this.languaje = languaje;
    }

    @Override
    public String getName() {
        String name = "";

        for (String n : fullName) {
            name += n + " ";
        }

        return name.trim();
    }

    public String getLanguaje() {
        return languaje;
    }

    public String getIsoCode() {
        return isoCode;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + fullName + '\'' +
                ", isoCode='" + isoCode + '\'' +
                ", languaje='" + languaje + '\'' +
                '}';
    }

    public Country cloneLocation() {
        return new Country(this.getName(), this.getIsoCode(), this.getLanguaje());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (!getName().equals(location.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Location l = (Location)o;
        return this.getName().compareTo(l.getName());
    }
}
