package com.mindrop.preprocessing.localization;

import java.util.ArrayList;
import java.util.List;

public class LocationQuerySegmentation {

    private List<String> locations;
    private String residualQuery;

    public LocationQuerySegmentation() {
        this.locations = new ArrayList<>();
        this.residualQuery = "";
    }

    public LocationQuerySegmentation(List<String> locations, String residualQuery) {
        this.locations = locations;
        this.residualQuery = residualQuery;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public String getResidualQuery() {
        return residualQuery;
    }

    public void setResidualQuery(String residualQuery) {
        this.residualQuery = residualQuery;
    }
}
