package com.mindrop.preprocessing.localization;

public interface Location {
    public static final String SPA_LANG = "spa";
    public static final String ENG_LANG = "eng";

    public String getName();
    public Location cloneLocation();
}
