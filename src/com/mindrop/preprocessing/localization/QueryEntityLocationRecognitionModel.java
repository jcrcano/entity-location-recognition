package com.mindrop.preprocessing.localization;

public interface QueryEntityLocationRecognitionModel {

    public LocationQuerySegmentation getSegments(String rawQuery);

}
