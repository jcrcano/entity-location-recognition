package com.mindrop.preprocessing.localization;


import java.util.Arrays;
import java.util.List;

public class AdminDivisionSecondLevel implements Location, Comparable {
    private List<String> fullName;
    private String isoCode;
    private String languaje;
    private String adminDivFirstLevel;

    public AdminDivisionSecondLevel(String name, String isoCode, String languaje, String adminDivFirstLevel) {
        fullName = Arrays.asList(name.split(" "));
        this.isoCode = isoCode;
        this.languaje = languaje;
        this.adminDivFirstLevel = adminDivFirstLevel;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getLanguaje() {
        return languaje;
    }

    public void setLanguaje(String languaje) {
        this.languaje = languaje;
    }

    public String getAdminDivFirstLevel() {
        return adminDivFirstLevel;
    }

    public void setAdminDivFirstLevel(String adminDivFirstLevel) {
        this.adminDivFirstLevel = adminDivFirstLevel;
    }

    @Override
    public String getName() {
        String name = "";

        for (String n : fullName) {
            name += n + " ";
        }

        return name.trim();
    }

    @Override
    public String toString() {
        return "AdminDivisionSecondLevel{" +
                "fullName=" + fullName +
                ", isoCode='" + isoCode + '\'' +
                ", languaje='" + languaje + '\'' +
                ", adminDivFirstLevel=" + adminDivFirstLevel +
                '}';
    }

    public Location cloneLocation() {
        return new AdminDivisionSecondLevel(this.getName(), this.getIsoCode(), this.getLanguaje(), this.getAdminDivFirstLevel());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location location = (Location) o;

        if (!getName().equals(location.getName())) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(Object o) {
        Location l = (Location) o;
        return this.getName().compareTo(l.getName());
    }
}
